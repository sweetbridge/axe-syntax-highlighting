cd C:\Users\charl\.vscode\extensions\axe.syntax
rd release/s /q
md release
cd release
copy ..\README.md
copy ..\package.json
copy ..\tsconfig.json
copy ..\package-lock.json
copy ..\CHANGELOG.md
copy ..\.eslintrc
robocopy ..\out C:\Users\charl\.vscode\extensions\axe.syntax\release\out\ /e
robocopy ..\src C:\Users\charl\.vscode\extensions\axe.syntax\release\src\ /e
robocopy ..\images C:\Users\charl\.vscode\extensions\axe.syntax\release\images\ /e
robocopy ..\syntaxes C:\Users\charl\.vscode\extensions\axe.syntax\release\syntaxes\ /e
robocopy ..\node_modules C:\Users\charl\.vscode\extensions\axe.syntax\release\node_modules\ /e

cd ..

cmd /k