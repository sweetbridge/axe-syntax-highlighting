# Axe Extension for VSCode

## Features

- Syntax highlighting for .axe files within VS Code.
- Automatic syntax checking and error highlighting
- Hover over an account number to view details of that account.
- Code completion for account numbers
  - Type the name of the account, and the code will be filled in for you
  - when you select the appropriate account in the completion dropdown.
- Code completion for roles and variables
- Hovering previews for roles and variables

## Notes
- The extension is only active when either the text file open has a .axe extension, or the file is of type "axe." You can change the type of the file in the bottom right of the VSCode window.  Click on the file type (it will say plaintext if freshly pasted), and select axe from the list (or type "axe").
- For more detail, look at the provided animation for instructions on use.

## Cloning this extension
For instructions on how to create a new VS Code extension like this one,
Check out [clone_instructions.md](https://bitbucket.org/sweetbridge/axe-syntax-highlighting/src/master/clone_instructions.md)

## Installation of the Axe VSCode Extension
1. Download the .zip and place the directory inside it into your ~/.vscode/extensions folder, with ~ representing your home folder.  (On a MAC, if your login is Bob, the ~ folder would be /Users/Bob/). Name the folder "axe-extension" or something similar.
2. In a terminal window, cd into the extension directory and run "npm install" to ensure you have the required packages.
3. Restart VS Code, and the extension should be operational.