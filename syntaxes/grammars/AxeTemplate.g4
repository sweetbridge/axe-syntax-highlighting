grammar AxeTemplate;
import AxeCommon;
  
template: header metaDef srcItem+;  

header: 
    ('#AxeLang:' ver=quotedStr)? 
    ('#Desc:' desc=quotedStr);

// meta declaration of variables
metaDef: ( '#declare' (r=metaRoleDef | v=metaVarDef))*;    
metaRoleDef: role=roleId desc=quotedStr?; 
metaVarDef: val=metaVarName type=varType desc=quotedStr?; 
metaVarName: ID | 'value' | 'VALUE' | 'currency' | 'CURRENCY' ; // Accept even reserved names (value and currency)
  
varType: t='TEXT' | n='NUM' | p='PERCENT' | dt='D-TYPE' | ct='C-TYPE' | un='UNIT' ; 

srcItem: 
    lbl=labelItem
    | role=roleVarItem
    | var=varDeclItem
    | inc=includeItem 
    | acc=accountStmt 
    | cass=commodityStmt 
    | dass=discreteStmt
    | cmt=commentItem
    ; 

commentItem: COMMENT+; 

// (1) `Some parametrized text`
labelItem: ('(' index=INT ')')? desc=quotedStr;

varDeclItem: name=varName '=' init=varInitExpr;
varName: id=ID | xid=EXT_ID  | ~'txdate' |  ~'value'; // excludes predefined vars, 
varInitExpr: (nval=numVarExpr | sval=strVarExpr);

// A role alias. Useful for the inclusion overriding
roleVarItem: alias=roleAlias '=' role=roleId; 

includeItem: '#include' tid=quotedStr ( localScope (',' localScope)* )?;
localScope: role=roleLocalScope | val=varLocalScope; 
roleLocalScope: inner=roleId '<-' outer=roleId;
varLocalScope: name=varName '<-' init=varInitExpr;  

// Accounts 
accountStmt: role=templateRole left=accountSetterList '||' right=accountSetterList;
accountSetterList: accountSetter (',' accountSetter)*;
accountSetter: accId=accountId title=quotedStr? val=value?;
accountId: number=accountNumber;


// Commoditites
commodityStmt: asset=commodityStmtAsset adj=assetAdjustment; 
commodityStmtAsset: 
        (swc=SWC swcQty=qty?) 
        | ( brc=BRC ( '/' brcUnit=unit )? brcQty=qty?)
        | (fiat=FIAT fiatQty=qty? fiatUnit=unit?)
        | (crypto=CRYPTO cryptoQty=qty? cryptoUnit=unit?)  
        | (virtual=VIRTUAL virtualQty=qty? virtualUnit=unit?)
        | (COMMODITY typeId=assetTypeId macQty=qty macUnit=unit)
    ;

// Discretes 
discreteStmt: DISCRETE asset=discreteStmtAsset adj=assetAdjustment; 
discreteStmtAsset: typeId=assetTypeId macAssetId=assetId macShare=share;

assetAdjustment: (role=templateRole  (incr='(+)' | decr='(-)'))  | (from=templateRole '->' to=templateRole); 
assetId: rf=VAR_REF; 

value: remainder='#' | num=extNum | form=expr;
unit: id=extId | rf=VAR_REF; 
qty: lit=extNum | form=expr;
share: lit=PERCENT | form=expr;
numVarExpr: form=expr | perc=PERCENT | noPerc=extNum;
strVarExpr: lit=quotedStr ;
expr: form=FORMULA | ref=VAR_REF;
templateRole: roleId | roleAlias;
roleAlias: ROLE_ALIAS_PREFIX id=extId; 
ROLE_ALIAS_PREFIX: '@@';
ROLE_PREFIX: '@';
VAR_REF: ':' (ID | EXT_ID);
FORMULA: '{' ~[{]*? '}';
COMMENT: '//' ~[\r\n]* ;


