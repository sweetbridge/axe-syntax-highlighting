grammar AxeCommon;

// can be @ROLE or @'Some Role'
roleId: '@' id=extId;

accountNumber: num=INT | id=ID | ~COMMODITY | ~DISCRETE | ~SWC | ~BRC | ~FIAT | ~CRYPTO | ~VIRTUAL ; 

// Integer or decimal
extNum: dec=DEC | int=INT;

// can be 'Some Id' or SomeId or Some.Id-Or-AnotherOne
extId: qs=quotedStr | id=ID | ext=EXT_ID; 

// 'string' or "string" or `string`
quotedStr: dq=DQ_STRING | sq=SQ_STRING | ts=TICK_STRING;

// Asset identifiers are symbols
assetTypeId: ID | EXT_ID | ~COMMODITY | ~DISCRETE | ~SWC | ~BRC | ~FIAT | ~CRYPTO | ~VIRTUAL;

// A signature, that contains all the possible items
signature: val=quotedStr ('by' signer=extId ('for' mandator=extId ('as' mandate=extId)?)?)?;  

COMMODITY: 'c-asset' | 'commodity' | '#c' | 'C-ASSET' | 'COMMODITY' | 'Commodity' | '#C'| '#fa' | '#FA';
DISCRETE: 'd-asset' | 'discrete' | '#d' | 'D-ASSET' | 'DISCRETE' | 'Discrete' | '#D' | '#da' | '#DA';
SWC: ('swc' | 'SWC' | 'Swc');
BRC: ('brc' | 'BRC' | 'Brc') ;
FIAT: ('fiat' | 'FIAT' | 'Fiat');
CRYPTO: ('crypto' | 'CRYPTO' | 'Crypto') ;
VIRTUAL: ('virtual' | 'VIRTUAL' | 'Virtual') ;

PERCENT: (INT | DEC) '%';
DEC: '-'? POS_DEC;
POS_DEC: [0-9]* '.' [0-9]+;
EXT_ID: ID (( '.' | '-') ID)+;
INT: '-'? POS_INT; 
POS_INT: [0-9]+;
ID: [a-zA-Z0-9_]+;
DQ_STRING: '"' ~["]*? '"';
SQ_STRING: '\'' ~[']*? '\'';
TICK_STRING: '`' ~[`]*? '`'; 

WHITESPACE : (' '|'\t')+ -> skip ;
NEWLINE : ('\r'? '\n' | '\r')+ -> skip;

