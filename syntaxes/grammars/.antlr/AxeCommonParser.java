// Generated from c:\Users\charl\.vscode\extensions\axe.syntax\syntaxes\grammars\AxeCommon.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AxeCommonParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, COMMODITY=5, DISCRETE=6, SWC=7, BRC=8, 
		FIAT=9, CRYPTO=10, VIRTUAL=11, PERCENT=12, DEC=13, POS_DEC=14, EXT_ID=15, 
		INT=16, POS_INT=17, ID=18, DQ_STRING=19, SQ_STRING=20, TICK_STRING=21, 
		WHITESPACE=22, NEWLINE=23;
	public static final int
		RULE_roleId = 0, RULE_accountNumber = 1, RULE_extNum = 2, RULE_extId = 3, 
		RULE_quotedStr = 4, RULE_assetTypeId = 5, RULE_signature = 6;
	private static String[] makeRuleNames() {
		return new String[] {
			"roleId", "accountNumber", "extNum", "extId", "quotedStr", "assetTypeId", 
			"signature"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'@'", "'by'", "'for'", "'as'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, "COMMODITY", "DISCRETE", "SWC", "BRC", 
			"FIAT", "CRYPTO", "VIRTUAL", "PERCENT", "DEC", "POS_DEC", "EXT_ID", "INT", 
			"POS_INT", "ID", "DQ_STRING", "SQ_STRING", "TICK_STRING", "WHITESPACE", 
			"NEWLINE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "AxeCommon.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public AxeCommonParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class RoleIdContext extends ParserRuleContext {
		public ExtIdContext id;
		public ExtIdContext extId() {
			return getRuleContext(ExtIdContext.class,0);
		}
		public RoleIdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_roleId; }
	}

	public final RoleIdContext roleId() throws RecognitionException {
		RoleIdContext _localctx = new RoleIdContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_roleId);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(14);
			match(T__0);
			setState(15);
			((RoleIdContext)_localctx).id = extId();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AccountNumberContext extends ParserRuleContext {
		public Token num;
		public Token id;
		public TerminalNode INT() { return getToken(AxeCommonParser.INT, 0); }
		public TerminalNode ID() { return getToken(AxeCommonParser.ID, 0); }
		public TerminalNode COMMODITY() { return getToken(AxeCommonParser.COMMODITY, 0); }
		public TerminalNode DISCRETE() { return getToken(AxeCommonParser.DISCRETE, 0); }
		public TerminalNode SWC() { return getToken(AxeCommonParser.SWC, 0); }
		public TerminalNode BRC() { return getToken(AxeCommonParser.BRC, 0); }
		public TerminalNode FIAT() { return getToken(AxeCommonParser.FIAT, 0); }
		public TerminalNode CRYPTO() { return getToken(AxeCommonParser.CRYPTO, 0); }
		public TerminalNode VIRTUAL() { return getToken(AxeCommonParser.VIRTUAL, 0); }
		public AccountNumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_accountNumber; }
	}

	public final AccountNumberContext accountNumber() throws RecognitionException {
		AccountNumberContext _localctx = new AccountNumberContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_accountNumber);
		int _la;
		try {
			setState(26);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(17);
				((AccountNumberContext)_localctx).num = match(INT);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(18);
				((AccountNumberContext)_localctx).id = match(ID);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(19);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==COMMODITY) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(20);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==DISCRETE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(21);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==SWC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(22);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==BRC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(23);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==FIAT) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(24);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==CRYPTO) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(25);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==VIRTUAL) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExtNumContext extends ParserRuleContext {
		public Token dec;
		public Token int;
		public TerminalNode DEC() { return getToken(AxeCommonParser.DEC, 0); }
		public TerminalNode INT() { return getToken(AxeCommonParser.INT, 0); }
		public ExtNumContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_extNum; }
	}

	public final ExtNumContext extNum() throws RecognitionException {
		ExtNumContext _localctx = new ExtNumContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_extNum);
		try {
			setState(30);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DEC:
				enterOuterAlt(_localctx, 1);
				{
				setState(28);
				((ExtNumContext)_localctx).dec = match(DEC);
				}
				break;
			case INT:
				enterOuterAlt(_localctx, 2);
				{
				setState(29);
				((ExtNumContext)_localctx).int = match(INT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExtIdContext extends ParserRuleContext {
		public QuotedStrContext qs;
		public Token id;
		public Token ext;
		public QuotedStrContext quotedStr() {
			return getRuleContext(QuotedStrContext.class,0);
		}
		public TerminalNode ID() { return getToken(AxeCommonParser.ID, 0); }
		public TerminalNode EXT_ID() { return getToken(AxeCommonParser.EXT_ID, 0); }
		public ExtIdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_extId; }
	}

	public final ExtIdContext extId() throws RecognitionException {
		ExtIdContext _localctx = new ExtIdContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_extId);
		try {
			setState(35);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DQ_STRING:
			case SQ_STRING:
			case TICK_STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(32);
				((ExtIdContext)_localctx).qs = quotedStr();
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(33);
				((ExtIdContext)_localctx).id = match(ID);
				}
				break;
			case EXT_ID:
				enterOuterAlt(_localctx, 3);
				{
				setState(34);
				((ExtIdContext)_localctx).ext = match(EXT_ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QuotedStrContext extends ParserRuleContext {
		public Token dq;
		public Token sq;
		public Token ts;
		public TerminalNode DQ_STRING() { return getToken(AxeCommonParser.DQ_STRING, 0); }
		public TerminalNode SQ_STRING() { return getToken(AxeCommonParser.SQ_STRING, 0); }
		public TerminalNode TICK_STRING() { return getToken(AxeCommonParser.TICK_STRING, 0); }
		public QuotedStrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quotedStr; }
	}

	public final QuotedStrContext quotedStr() throws RecognitionException {
		QuotedStrContext _localctx = new QuotedStrContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_quotedStr);
		try {
			setState(40);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DQ_STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(37);
				((QuotedStrContext)_localctx).dq = match(DQ_STRING);
				}
				break;
			case SQ_STRING:
				enterOuterAlt(_localctx, 2);
				{
				setState(38);
				((QuotedStrContext)_localctx).sq = match(SQ_STRING);
				}
				break;
			case TICK_STRING:
				enterOuterAlt(_localctx, 3);
				{
				setState(39);
				((QuotedStrContext)_localctx).ts = match(TICK_STRING);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssetTypeIdContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(AxeCommonParser.ID, 0); }
		public TerminalNode EXT_ID() { return getToken(AxeCommonParser.EXT_ID, 0); }
		public TerminalNode COMMODITY() { return getToken(AxeCommonParser.COMMODITY, 0); }
		public TerminalNode DISCRETE() { return getToken(AxeCommonParser.DISCRETE, 0); }
		public TerminalNode SWC() { return getToken(AxeCommonParser.SWC, 0); }
		public TerminalNode BRC() { return getToken(AxeCommonParser.BRC, 0); }
		public TerminalNode FIAT() { return getToken(AxeCommonParser.FIAT, 0); }
		public TerminalNode CRYPTO() { return getToken(AxeCommonParser.CRYPTO, 0); }
		public TerminalNode VIRTUAL() { return getToken(AxeCommonParser.VIRTUAL, 0); }
		public AssetTypeIdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assetTypeId; }
	}

	public final AssetTypeIdContext assetTypeId() throws RecognitionException {
		AssetTypeIdContext _localctx = new AssetTypeIdContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_assetTypeId);
		int _la;
		try {
			setState(51);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(42);
				match(ID);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(43);
				match(EXT_ID);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(44);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==COMMODITY) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(45);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==DISCRETE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(46);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==SWC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(47);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==BRC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(48);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==FIAT) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(49);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==CRYPTO) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(50);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==VIRTUAL) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SignatureContext extends ParserRuleContext {
		public QuotedStrContext val;
		public ExtIdContext signer;
		public ExtIdContext mandator;
		public ExtIdContext mandate;
		public QuotedStrContext quotedStr() {
			return getRuleContext(QuotedStrContext.class,0);
		}
		public List<ExtIdContext> extId() {
			return getRuleContexts(ExtIdContext.class);
		}
		public ExtIdContext extId(int i) {
			return getRuleContext(ExtIdContext.class,i);
		}
		public SignatureContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signature; }
	}

	public final SignatureContext signature() throws RecognitionException {
		SignatureContext _localctx = new SignatureContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_signature);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			((SignatureContext)_localctx).val = quotedStr();
			setState(64);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(54);
				match(T__1);
				setState(55);
				((SignatureContext)_localctx).signer = extId();
				setState(62);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__2) {
					{
					setState(56);
					match(T__2);
					setState(57);
					((SignatureContext)_localctx).mandator = extId();
					setState(60);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==T__3) {
						{
						setState(58);
						match(T__3);
						setState(59);
						((SignatureContext)_localctx).mandate = extId();
						}
					}

					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\31E\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\3\2\3\2\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\5\3\35\n\3\3\4\3\4\5\4!\n\4\3\5\3\5\3\5\5\5&\n\5"+
		"\3\6\3\6\3\6\5\6+\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\66\n\7\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b?\n\b\5\bA\n\b\5\bC\n\b\3\b\2\2\t\2\4\6"+
		"\b\n\f\16\2\t\3\2\7\7\3\2\b\b\3\2\t\t\3\2\n\n\3\2\13\13\3\2\f\f\3\2\r"+
		"\r\2U\2\20\3\2\2\2\4\34\3\2\2\2\6 \3\2\2\2\b%\3\2\2\2\n*\3\2\2\2\f\65"+
		"\3\2\2\2\16\67\3\2\2\2\20\21\7\3\2\2\21\22\5\b\5\2\22\3\3\2\2\2\23\35"+
		"\7\22\2\2\24\35\7\24\2\2\25\35\n\2\2\2\26\35\n\3\2\2\27\35\n\4\2\2\30"+
		"\35\n\5\2\2\31\35\n\6\2\2\32\35\n\7\2\2\33\35\n\b\2\2\34\23\3\2\2\2\34"+
		"\24\3\2\2\2\34\25\3\2\2\2\34\26\3\2\2\2\34\27\3\2\2\2\34\30\3\2\2\2\34"+
		"\31\3\2\2\2\34\32\3\2\2\2\34\33\3\2\2\2\35\5\3\2\2\2\36!\7\17\2\2\37!"+
		"\7\22\2\2 \36\3\2\2\2 \37\3\2\2\2!\7\3\2\2\2\"&\5\n\6\2#&\7\24\2\2$&\7"+
		"\21\2\2%\"\3\2\2\2%#\3\2\2\2%$\3\2\2\2&\t\3\2\2\2\'+\7\25\2\2(+\7\26\2"+
		"\2)+\7\27\2\2*\'\3\2\2\2*(\3\2\2\2*)\3\2\2\2+\13\3\2\2\2,\66\7\24\2\2"+
		"-\66\7\21\2\2.\66\n\2\2\2/\66\n\3\2\2\60\66\n\4\2\2\61\66\n\5\2\2\62\66"+
		"\n\6\2\2\63\66\n\7\2\2\64\66\n\b\2\2\65,\3\2\2\2\65-\3\2\2\2\65.\3\2\2"+
		"\2\65/\3\2\2\2\65\60\3\2\2\2\65\61\3\2\2\2\65\62\3\2\2\2\65\63\3\2\2\2"+
		"\65\64\3\2\2\2\66\r\3\2\2\2\67B\5\n\6\289\7\4\2\29@\5\b\5\2:;\7\5\2\2"+
		";>\5\b\5\2<=\7\6\2\2=?\5\b\5\2><\3\2\2\2>?\3\2\2\2?A\3\2\2\2@:\3\2\2\2"+
		"@A\3\2\2\2AC\3\2\2\2B8\3\2\2\2BC\3\2\2\2C\17\3\2\2\2\n\34 %*\65>@B";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}