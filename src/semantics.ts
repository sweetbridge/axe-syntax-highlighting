import * as vscode from 'vscode';
import * as extension from './extension';
import * as parser from './parser';
import { ANTLRInputStream, CommonTokenStream } from 'antlr4ts';
import * as axe_lexer from './grammar/AxeTemplateLexer';
import * as axe_listener from './grammar/AxeTemplateListener';
import * as axe_parser from './grammar/AxeTemplateParser';
import * as hover from './hover';
import * as completion from './completion';

// #region Create legend for semantics
// List of colors for default dark+ https://github.com/microsoft/vscode/blob/main/extensions/theme-defaults/themes/dark_plus.json
const tokenTypesLegend = [
    'class', 'variable', 'property', 'enumMember', 'function', 'string', 'keyword'
];
const tokenModifiersLegend = [
    'declaration'
];
export const legend = new vscode.SemanticTokensLegend(tokenTypesLegend, tokenModifiersLegend);
// #endregion

// Declare the builder, diagnostics, and current file as export vars so
// they are accessible from the listener in parser.ts
export var builder = new vscode.SemanticTokensBuilder(legend);
export var diagnostics:parser.AxeDiagnostic[] = [];
export var currentFile:vscode.Uri;

const parseListener:axe_listener.AxeTemplateListener = new parser.ParseListener();
const errorParserListener:parser.ErrorParserListener = new parser.ErrorParserListener();

export class AxeSemanticTokensProvider implements vscode.DocumentSemanticTokensProvider 
{
    // Main override method
    provideDocumentSemanticTokens(document: vscode.TextDocument, token: vscode.CancellationToken): vscode.ProviderResult<vscode.SemanticTokens>
    {
        AxeSemanticTokensProvider.performDiagnostics(document.getText(), document.uri);
        return builder.build();
    }

    // Utility method, called by provideDocumentSemanticTokens()
    // Copied from essl-extension semantics.ts where this method is called
    // at activation for every open .essl file, to show diagnostics in files that
    // have not been opened yet.
    public static performDiagnostics(document: string, documentPath: vscode.Uri)
    {
        // Make the current file available by the listener
        currentFile = documentPath;

        // Reset the semantics builder
        builder = new vscode.SemanticTokensBuilder(legend);

        // Parse the doc and provide semantics/diagnostics to the
        // static builders declared above (builder and diagnostics)
        // Also update the list of regions for code completion
        AxeSemanticTokensProvider.updateRegions(document);

        // By this point, the listeners have finished collecting info.
        // Send the client diagnostics, and return the finished builder.
        extension.diagnosticCollection.set(documentPath, diagnostics);
    }

    // Parse the doc and provide semantics
    // Also update the list of regions for code completion
    public static updateRegions(text: string)
    {
        // Declare the Axe Parser/Lexer
        let inputStream = new ANTLRInputStream(text!);
        let axeLexer = new axe_lexer.AxeTemplateLexer(inputStream);
        let tokenStream = new CommonTokenStream(axeLexer);
        let axeParser = new axe_parser.AxeTemplateParser(tokenStream);

        // Remove the predefined console listeners to prevent excessive tracing
        axeParser.removeParseListeners();
        axeParser.removeErrorListeners();
        axeLexer.removeErrorListeners();

        // Add the listeners to the parser
        // ParseListener is in charge of syntax highlighting, and uses the exported builder variable
        // ErrorListener is in charge of diagnostics, and uses the exported diagnostics variable
        axeLexer.addErrorListener(errorParserListener);
        axeParser.addParseListener(parseListener);
        axeParser.addErrorListener(errorParserListener);

        // Reset the public lists
        diagnostics = [];
        hover.accountNumberPositions.splice(0, hover.accountNumberPositions.length);
        completion.templateRolePositions.splice(0, completion.templateRolePositions.length);
        hover.metaDefs.splice(0, hover.metaDefs.length);
        parser.metaVarPositions.splice(0,parser.metaVarPositions.length);
        parser.metaNames.splice(0,parser.metaNames.length);

        // Parse the Axe file, starting at template (declared in AxeTemplate.g4 as the start)
        // Listener will begin detecting
        axeParser.template();
    }
    
}