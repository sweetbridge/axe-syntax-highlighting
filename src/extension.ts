// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as semantics from './semantics';
import * as completion from './completion';
import * as hover from './hover';

// Publicly visible list of diagnostics for the client to use
// This provides the user with error signals, red squigglies, and
// problems reported in the "problems" tab at the bottom
export var diagnosticCollection: vscode.DiagnosticCollection;

// vscode.DocumentSelector for the providers below, so they're only active on axe files
const selector = {language: 'axe'};

// This method is called when the extension is activated.
// See activation events in package.json
export function activate(context: vscode.ExtensionContext)
{
	// #region Register document semantics provider
	vscode.languages.registerDocumentSemanticTokensProvider(selector, new semantics.AxeSemanticTokensProvider(), semantics.legend);
	// #endregion

	// #region Register diagnostics provider
	diagnosticCollection = vscode.languages.createDiagnosticCollection('axe');
	context.subscriptions.push(diagnosticCollection);
	// #endregion

	// #region Register code completion provider
	// Include characters at the end that represent need code completion after them
	var completionProvider = vscode.languages.registerCompletionItemProvider(
		selector,
		new completion.AxeCompletionItemProvider(),
		'|', '.', '\"', '', '\n', ' ', '@', ':', "`", "'", "\""
	);
	context.subscriptions.push(completionProvider);
	// Load the .json data files that hold static completion data
	completion.initAccountsCompletionList();
	//#endregion

	// #region Register code hovering provider
	var hoverProvider = vscode.languages.registerHoverProvider(selector, new hover.AxeHoverProvider());
	context.subscriptions.push(hoverProvider);
	// #endregion
}

// this method is called when your extension is deactivated
export function deactivate() {}
