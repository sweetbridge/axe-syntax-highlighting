import * as vscode from 'vscode';
import * as completion from './completion';

// List of all account numbers in the currently opened axe file
export var accountNumberPositions:AccountNumber[] = [];

export class AxeHoverProvider implements vscode.HoverProvider
{
    provideHover(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken): vscode.ProviderResult<vscode.Hover>
    {
        // Adjust the given position to be the start of the identifier. (To properly match its position in accountNumberPositions array)
        // If undefined, user is not hovering on an identifier, so return.
        var adjustedChar:number | undefined = getPositionOfIdentifierStart(document, position, /[a-zA-Z0-9_]/);
        if (adjustedChar === undefined)
        {
            return;
        }

        // Declare the new position of the hovered location
        var adjustedPosition:vscode.Position = new vscode.Position(position.line, adjustedChar!);

        // Check if the currently hovered identifier is a known accountNumber or variable
        for (var i=0; i<accountNumberPositions.length; i++)
        {
            var pos = accountNumberPositions[i].position;

            if (pos.isEqual(adjustedPosition))
            {
                // At this point, we know that we have an accountNumber.
                // Now we just need to pull the code from the accountNumber, 
                // and provide a hover in the form of a markdown string to VSCode
                var code = accountNumberPositions[i].code;
                for (var j=0; j<completion.accounts.length; j++)
                {
                    var account = completion.accounts[j];
                    if (account.New_Code === code)
                    {
                        // Build the markdown string and return it as a hover
                        var mkString = new vscode.MarkdownString(`**Account**: ${account.Account}\n\n*${account.getFullAccountPath()}*\n\n**Type**: ${account.Cash_Flow_Category}, ${account.Natural_Balance}\n\n**Description**: ${account.Description}`);
                        return new vscode.Hover(mkString);
                    }
                }
            }
        }

        var expression = /^[a-zA-Z0-9_]+/;
        for (var i=0; i<metaDefs.length; i++)
        {
            var metaDef = metaDefs[i];

            var line = document.getText().split("\n")[adjustedPosition.line].substring(adjustedPosition.character);
            var match = line.match(expression);

            if (match === null)
            {
                continue;
            }

            if (metaDef.name === match[0])
            {
                // At this point, we know that we have a metadef. Now just provide it as a hover
                var mkString = new vscode.MarkdownString(`**${metaDef.name}**: ${(metaDef.type) ?? "ROLE"}\n\n${metaDef.comment}`);
                return new vscode.Hover(mkString);
            }
        }

        // The currently hovered identifier is not an account number, so
        // do not provide any hover.
        return;
    }
}

// Adjust the given position to be the start of the identifier.
// Return undefined if the index is not an identifier.
export function getPositionOfIdentifierStart(document: vscode.TextDocument, position:vscode.Position, customMatch:RegExp = /[a-zA-Z0-9_]/) : number | undefined
{
    var line = document.getText().split("\n")[position.line];
    var index = position.character;

    while (index >= 0)
    {
        if (!line.charAt(index).match(customMatch))
        {
            return index === position.character ? undefined : index+1;
        }

        index-=1;
    }

    return undefined;
}

// Util class, used by hover provider
// Stores the code of the accountNumber and its position in the file (line, char)
export class AccountNumber
{
    public position:vscode.Position;
    public code:string;

    constructor(_position:vscode.Position, _code:string)
    {
        this.position = _position;
        this.code = _code;
    }
}

// Static list of meta defs, used by hover and completion
export var metaDefs:MetaDef[] = [];

// Util class, used by hover provider
// Stores the meta variable definitions and their positions in the file
export class MetaDef
{
    public position:vscode.Position;
    public name:string;
    public type:string;
    public comment:string;

    public isVar:boolean;

    constructor(_position:vscode.Position, _name:string, _type:string, _comment:string)
    {
        this.position = _position;
        this.name = _name;
        this.type = _type;
        this.comment = _comment;

        this.isVar = this.type !== undefined;
    }
}