// Generated from syntaxes/grammars/AxeTemplate.g4 by ANTLR 4.9.0-SNAPSHOT


import { ATN } from "antlr4ts/atn/ATN";
import { ATNDeserializer } from "antlr4ts/atn/ATNDeserializer";
import { FailedPredicateException } from "antlr4ts/FailedPredicateException";
import { NotNull } from "antlr4ts/Decorators";
import { NoViableAltException } from "antlr4ts/NoViableAltException";
import { Override } from "antlr4ts/Decorators";
import { Parser } from "antlr4ts/Parser";
import { ParserRuleContext } from "antlr4ts/ParserRuleContext";
import { ParserATNSimulator } from "antlr4ts/atn/ParserATNSimulator";
import { ParseTreeListener } from "antlr4ts/tree/ParseTreeListener";
import { ParseTreeVisitor } from "antlr4ts/tree/ParseTreeVisitor";
import { RecognitionException } from "antlr4ts/RecognitionException";
import { RuleContext } from "antlr4ts/RuleContext";
//import { RuleVersion } from "antlr4ts/RuleVersion";
import { TerminalNode } from "antlr4ts/tree/TerminalNode";
import { Token } from "antlr4ts/Token";
import { TokenStream } from "antlr4ts/TokenStream";
import { Vocabulary } from "antlr4ts/Vocabulary";
import { VocabularyImpl } from "antlr4ts/VocabularyImpl";

import * as Utils from "antlr4ts/misc/Utils";

import { AxeTemplateListener } from "./AxeTemplateListener";
import { AxeTemplateVisitor } from "./AxeTemplateVisitor";


export class AxeTemplateParser extends Parser {
	public static readonly T__0 = 1;
	public static readonly T__1 = 2;
	public static readonly T__2 = 3;
	public static readonly T__3 = 4;
	public static readonly T__4 = 5;
	public static readonly T__5 = 6;
	public static readonly T__6 = 7;
	public static readonly T__7 = 8;
	public static readonly T__8 = 9;
	public static readonly T__9 = 10;
	public static readonly T__10 = 11;
	public static readonly T__11 = 12;
	public static readonly T__12 = 13;
	public static readonly T__13 = 14;
	public static readonly T__14 = 15;
	public static readonly T__15 = 16;
	public static readonly T__16 = 17;
	public static readonly T__17 = 18;
	public static readonly T__18 = 19;
	public static readonly T__19 = 20;
	public static readonly T__20 = 21;
	public static readonly T__21 = 22;
	public static readonly T__22 = 23;
	public static readonly T__23 = 24;
	public static readonly T__24 = 25;
	public static readonly T__25 = 26;
	public static readonly T__26 = 27;
	public static readonly T__27 = 28;
	public static readonly T__28 = 29;
	public static readonly ROLE_ALIAS_PREFIX = 30;
	public static readonly ROLE_PREFIX = 31;
	public static readonly VAR_REF = 32;
	public static readonly FORMULA = 33;
	public static readonly COMMENT = 34;
	public static readonly COMMODITY = 35;
	public static readonly DISCRETE = 36;
	public static readonly SWC = 37;
	public static readonly BRC = 38;
	public static readonly FIAT = 39;
	public static readonly CRYPTO = 40;
	public static readonly VIRTUAL = 41;
	public static readonly PERCENT = 42;
	public static readonly DEC = 43;
	public static readonly POS_DEC = 44;
	public static readonly EXT_ID = 45;
	public static readonly INT = 46;
	public static readonly POS_INT = 47;
	public static readonly ID = 48;
	public static readonly DQ_STRING = 49;
	public static readonly SQ_STRING = 50;
	public static readonly TICK_STRING = 51;
	public static readonly WHITESPACE = 52;
	public static readonly NEWLINE = 53;
	public static readonly RULE_template = 0;
	public static readonly RULE_header = 1;
	public static readonly RULE_metaDef = 2;
	public static readonly RULE_metaRoleDef = 3;
	public static readonly RULE_metaVarDef = 4;
	public static readonly RULE_metaVarName = 5;
	public static readonly RULE_varType = 6;
	public static readonly RULE_srcItem = 7;
	public static readonly RULE_commentItem = 8;
	public static readonly RULE_labelItem = 9;
	public static readonly RULE_varDeclItem = 10;
	public static readonly RULE_varName = 11;
	public static readonly RULE_varInitExpr = 12;
	public static readonly RULE_roleVarItem = 13;
	public static readonly RULE_includeItem = 14;
	public static readonly RULE_localScope = 15;
	public static readonly RULE_roleLocalScope = 16;
	public static readonly RULE_varLocalScope = 17;
	public static readonly RULE_accountStmt = 18;
	public static readonly RULE_accountSetterList = 19;
	public static readonly RULE_accountSetter = 20;
	public static readonly RULE_accountId = 21;
	public static readonly RULE_commodityStmt = 22;
	public static readonly RULE_commodityStmtAsset = 23;
	public static readonly RULE_discreteStmt = 24;
	public static readonly RULE_discreteStmtAsset = 25;
	public static readonly RULE_assetAdjustment = 26;
	public static readonly RULE_assetId = 27;
	public static readonly RULE_value = 28;
	public static readonly RULE_unit = 29;
	public static readonly RULE_qty = 30;
	public static readonly RULE_share = 31;
	public static readonly RULE_numVarExpr = 32;
	public static readonly RULE_strVarExpr = 33;
	public static readonly RULE_expr = 34;
	public static readonly RULE_templateRole = 35;
	public static readonly RULE_roleAlias = 36;
	public static readonly RULE_roleId = 37;
	public static readonly RULE_accountNumber = 38;
	public static readonly RULE_extNum = 39;
	public static readonly RULE_extId = 40;
	public static readonly RULE_quotedStr = 41;
	public static readonly RULE_assetTypeId = 42;
	public static readonly RULE_signature = 43;
	// tslint:disable:no-trailing-whitespace
	public static readonly ruleNames: string[] = [
		"template", "header", "metaDef", "metaRoleDef", "metaVarDef", "metaVarName", 
		"varType", "srcItem", "commentItem", "labelItem", "varDeclItem", "varName", 
		"varInitExpr", "roleVarItem", "includeItem", "localScope", "roleLocalScope", 
		"varLocalScope", "accountStmt", "accountSetterList", "accountSetter", 
		"accountId", "commodityStmt", "commodityStmtAsset", "discreteStmt", "discreteStmtAsset", 
		"assetAdjustment", "assetId", "value", "unit", "qty", "share", "numVarExpr", 
		"strVarExpr", "expr", "templateRole", "roleAlias", "roleId", "accountNumber", 
		"extNum", "extId", "quotedStr", "assetTypeId", "signature",
	];

	private static readonly _LITERAL_NAMES: Array<string | undefined> = [
		undefined, "'#AxeLang:'", "'#Desc:'", "'#declare'", "'value'", "'VALUE'", 
		"'currency'", "'CURRENCY'", "'TEXT'", "'NUM'", "'PERCENT'", "'D-TYPE'", 
		"'C-TYPE'", "'UNIT'", "'('", "')'", "'='", "'txdate'", "'#include'", "','", 
		"'<-'", "'||'", "'/'", "'(+)'", "'(-)'", "'->'", "'#'", "'by'", "'for'", 
		"'as'", "'@@'", "'@'",
	];
	private static readonly _SYMBOLIC_NAMES: Array<string | undefined> = [
		undefined, undefined, undefined, undefined, undefined, undefined, undefined, 
		undefined, undefined, undefined, undefined, undefined, undefined, undefined, 
		undefined, undefined, undefined, undefined, undefined, undefined, undefined, 
		undefined, undefined, undefined, undefined, undefined, undefined, undefined, 
		undefined, undefined, "ROLE_ALIAS_PREFIX", "ROLE_PREFIX", "VAR_REF", "FORMULA", 
		"COMMENT", "COMMODITY", "DISCRETE", "SWC", "BRC", "FIAT", "CRYPTO", "VIRTUAL", 
		"PERCENT", "DEC", "POS_DEC", "EXT_ID", "INT", "POS_INT", "ID", "DQ_STRING", 
		"SQ_STRING", "TICK_STRING", "WHITESPACE", "NEWLINE",
	];
	public static readonly VOCABULARY: Vocabulary = new VocabularyImpl(AxeTemplateParser._LITERAL_NAMES, AxeTemplateParser._SYMBOLIC_NAMES, []);

	// @Override
	// @NotNull
	public get vocabulary(): Vocabulary {
		return AxeTemplateParser.VOCABULARY;
	}
	// tslint:enable:no-trailing-whitespace

	// @Override
	public get grammarFileName(): string { return "AxeTemplate.g4"; }

	// @Override
	public get ruleNames(): string[] { return AxeTemplateParser.ruleNames; }

	// @Override
	public get serializedATN(): string { return AxeTemplateParser._serializedATN; }

	protected createFailedPredicateException(predicate?: string, message?: string): FailedPredicateException {
		return new FailedPredicateException(this, predicate, message);
	}

	constructor(input: TokenStream) {
		super(input);
		this._interp = new ParserATNSimulator(AxeTemplateParser._ATN, this);
	}
	// @RuleVersion(0)
	public template(): TemplateContext {
		let _localctx: TemplateContext = new TemplateContext(this._ctx, this.state);
		this.enterRule(_localctx, 0, AxeTemplateParser.RULE_template);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 88;
			this.header();
			this.state = 89;
			this.metaDef();
			this.state = 91;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			do {
				{
				{
				this.state = 90;
				this.srcItem();
				}
				}
				this.state = 93;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			} while ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << AxeTemplateParser.T__0) | (1 << AxeTemplateParser.T__1) | (1 << AxeTemplateParser.T__2) | (1 << AxeTemplateParser.T__3) | (1 << AxeTemplateParser.T__4) | (1 << AxeTemplateParser.T__5) | (1 << AxeTemplateParser.T__6) | (1 << AxeTemplateParser.T__7) | (1 << AxeTemplateParser.T__8) | (1 << AxeTemplateParser.T__9) | (1 << AxeTemplateParser.T__10) | (1 << AxeTemplateParser.T__11) | (1 << AxeTemplateParser.T__12) | (1 << AxeTemplateParser.T__13) | (1 << AxeTemplateParser.T__14) | (1 << AxeTemplateParser.T__15) | (1 << AxeTemplateParser.T__16) | (1 << AxeTemplateParser.T__17) | (1 << AxeTemplateParser.T__18) | (1 << AxeTemplateParser.T__19) | (1 << AxeTemplateParser.T__20) | (1 << AxeTemplateParser.T__21) | (1 << AxeTemplateParser.T__22) | (1 << AxeTemplateParser.T__23) | (1 << AxeTemplateParser.T__24) | (1 << AxeTemplateParser.T__25) | (1 << AxeTemplateParser.T__26) | (1 << AxeTemplateParser.T__27) | (1 << AxeTemplateParser.T__28) | (1 << AxeTemplateParser.ROLE_ALIAS_PREFIX) | (1 << AxeTemplateParser.ROLE_PREFIX))) !== 0) || ((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (AxeTemplateParser.VAR_REF - 32)) | (1 << (AxeTemplateParser.FORMULA - 32)) | (1 << (AxeTemplateParser.COMMENT - 32)) | (1 << (AxeTemplateParser.COMMODITY - 32)) | (1 << (AxeTemplateParser.DISCRETE - 32)) | (1 << (AxeTemplateParser.SWC - 32)) | (1 << (AxeTemplateParser.BRC - 32)) | (1 << (AxeTemplateParser.FIAT - 32)) | (1 << (AxeTemplateParser.CRYPTO - 32)) | (1 << (AxeTemplateParser.VIRTUAL - 32)) | (1 << (AxeTemplateParser.PERCENT - 32)) | (1 << (AxeTemplateParser.DEC - 32)) | (1 << (AxeTemplateParser.POS_DEC - 32)) | (1 << (AxeTemplateParser.EXT_ID - 32)) | (1 << (AxeTemplateParser.INT - 32)) | (1 << (AxeTemplateParser.POS_INT - 32)) | (1 << (AxeTemplateParser.ID - 32)) | (1 << (AxeTemplateParser.DQ_STRING - 32)) | (1 << (AxeTemplateParser.SQ_STRING - 32)) | (1 << (AxeTemplateParser.TICK_STRING - 32)) | (1 << (AxeTemplateParser.WHITESPACE - 32)) | (1 << (AxeTemplateParser.NEWLINE - 32)))) !== 0));
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public header(): HeaderContext {
		let _localctx: HeaderContext = new HeaderContext(this._ctx, this.state);
		this.enterRule(_localctx, 2, AxeTemplateParser.RULE_header);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 97;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === AxeTemplateParser.T__0) {
				{
				this.state = 95;
				this.match(AxeTemplateParser.T__0);
				this.state = 96;
				_localctx._ver = this.quotedStr();
				}
			}

			{
			this.state = 99;
			this.match(AxeTemplateParser.T__1);
			this.state = 100;
			_localctx._desc = this.quotedStr();
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public metaDef(): MetaDefContext {
		let _localctx: MetaDefContext = new MetaDefContext(this._ctx, this.state);
		this.enterRule(_localctx, 4, AxeTemplateParser.RULE_metaDef);
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 109;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input, 3, this._ctx);
			while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
				if (_alt === 1) {
					{
					{
					this.state = 102;
					this.match(AxeTemplateParser.T__2);
					this.state = 105;
					this._errHandler.sync(this);
					switch (this._input.LA(1)) {
					case AxeTemplateParser.ROLE_PREFIX:
						{
						this.state = 103;
						_localctx._r = this.metaRoleDef();
						}
						break;
					case AxeTemplateParser.T__3:
					case AxeTemplateParser.T__4:
					case AxeTemplateParser.T__5:
					case AxeTemplateParser.T__6:
					case AxeTemplateParser.ID:
						{
						this.state = 104;
						_localctx._v = this.metaVarDef();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
					}
				}
				this.state = 111;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 3, this._ctx);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public metaRoleDef(): MetaRoleDefContext {
		let _localctx: MetaRoleDefContext = new MetaRoleDefContext(this._ctx, this.state);
		this.enterRule(_localctx, 6, AxeTemplateParser.RULE_metaRoleDef);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 112;
			_localctx._role = this.roleId();
			this.state = 114;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 4, this._ctx) ) {
			case 1:
				{
				this.state = 113;
				_localctx._desc = this.quotedStr();
				}
				break;
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public metaVarDef(): MetaVarDefContext {
		let _localctx: MetaVarDefContext = new MetaVarDefContext(this._ctx, this.state);
		this.enterRule(_localctx, 8, AxeTemplateParser.RULE_metaVarDef);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 116;
			_localctx._val = this.metaVarName();
			this.state = 117;
			_localctx._type = this.varType();
			this.state = 119;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 5, this._ctx) ) {
			case 1:
				{
				this.state = 118;
				_localctx._desc = this.quotedStr();
				}
				break;
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public metaVarName(): MetaVarNameContext {
		let _localctx: MetaVarNameContext = new MetaVarNameContext(this._ctx, this.state);
		this.enterRule(_localctx, 10, AxeTemplateParser.RULE_metaVarName);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 121;
			_la = this._input.LA(1);
			if (!((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << AxeTemplateParser.T__3) | (1 << AxeTemplateParser.T__4) | (1 << AxeTemplateParser.T__5) | (1 << AxeTemplateParser.T__6))) !== 0) || _la === AxeTemplateParser.ID)) {
			this._errHandler.recoverInline(this);
			} else {
				if (this._input.LA(1) === Token.EOF) {
					this.matchedEOF = true;
				}

				this._errHandler.reportMatch(this);
				this.consume();
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public varType(): VarTypeContext {
		let _localctx: VarTypeContext = new VarTypeContext(this._ctx, this.state);
		this.enterRule(_localctx, 12, AxeTemplateParser.RULE_varType);
		try {
			this.state = 129;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.T__7:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 123;
				_localctx._t = this.match(AxeTemplateParser.T__7);
				}
				break;
			case AxeTemplateParser.T__8:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 124;
				_localctx._n = this.match(AxeTemplateParser.T__8);
				}
				break;
			case AxeTemplateParser.T__9:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 125;
				_localctx._p = this.match(AxeTemplateParser.T__9);
				}
				break;
			case AxeTemplateParser.T__10:
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 126;
				_localctx._dt = this.match(AxeTemplateParser.T__10);
				}
				break;
			case AxeTemplateParser.T__11:
				this.enterOuterAlt(_localctx, 5);
				{
				this.state = 127;
				_localctx._ct = this.match(AxeTemplateParser.T__11);
				}
				break;
			case AxeTemplateParser.T__12:
				this.enterOuterAlt(_localctx, 6);
				{
				this.state = 128;
				_localctx._un = this.match(AxeTemplateParser.T__12);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public srcItem(): SrcItemContext {
		let _localctx: SrcItemContext = new SrcItemContext(this._ctx, this.state);
		this.enterRule(_localctx, 14, AxeTemplateParser.RULE_srcItem);
		try {
			this.state = 139;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 7, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 131;
				_localctx._lbl = this.labelItem();
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 132;
				_localctx._role = this.roleVarItem();
				}
				break;

			case 3:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 133;
				_localctx._var = this.varDeclItem();
				}
				break;

			case 4:
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 134;
				_localctx._inc = this.includeItem();
				}
				break;

			case 5:
				this.enterOuterAlt(_localctx, 5);
				{
				this.state = 135;
				_localctx._acc = this.accountStmt();
				}
				break;

			case 6:
				this.enterOuterAlt(_localctx, 6);
				{
				this.state = 136;
				_localctx._cass = this.commodityStmt();
				}
				break;

			case 7:
				this.enterOuterAlt(_localctx, 7);
				{
				this.state = 137;
				_localctx._dass = this.discreteStmt();
				}
				break;

			case 8:
				this.enterOuterAlt(_localctx, 8);
				{
				this.state = 138;
				_localctx._cmt = this.commentItem();
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public commentItem(): CommentItemContext {
		let _localctx: CommentItemContext = new CommentItemContext(this._ctx, this.state);
		this.enterRule(_localctx, 16, AxeTemplateParser.RULE_commentItem);
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 142;
			this._errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					this.state = 141;
					this.match(AxeTemplateParser.COMMENT);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				this.state = 144;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 8, this._ctx);
			} while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public labelItem(): LabelItemContext {
		let _localctx: LabelItemContext = new LabelItemContext(this._ctx, this.state);
		this.enterRule(_localctx, 18, AxeTemplateParser.RULE_labelItem);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 149;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === AxeTemplateParser.T__13) {
				{
				this.state = 146;
				this.match(AxeTemplateParser.T__13);
				this.state = 147;
				_localctx._index = this.match(AxeTemplateParser.INT);
				this.state = 148;
				this.match(AxeTemplateParser.T__14);
				}
			}

			this.state = 151;
			_localctx._desc = this.quotedStr();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public varDeclItem(): VarDeclItemContext {
		let _localctx: VarDeclItemContext = new VarDeclItemContext(this._ctx, this.state);
		this.enterRule(_localctx, 20, AxeTemplateParser.RULE_varDeclItem);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 153;
			_localctx._name = this.varName();
			this.state = 154;
			this.match(AxeTemplateParser.T__15);
			this.state = 155;
			_localctx._init = this.varInitExpr();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public varName(): VarNameContext {
		let _localctx: VarNameContext = new VarNameContext(this._ctx, this.state);
		this.enterRule(_localctx, 22, AxeTemplateParser.RULE_varName);
		let _la: number;
		try {
			this.state = 161;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 10, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 157;
				_localctx._id = this.match(AxeTemplateParser.ID);
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 158;
				_localctx._xid = this.match(AxeTemplateParser.EXT_ID);
				}
				break;

			case 3:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 159;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.T__16)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 4:
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 160;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.T__3)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public varInitExpr(): VarInitExprContext {
		let _localctx: VarInitExprContext = new VarInitExprContext(this._ctx, this.state);
		this.enterRule(_localctx, 24, AxeTemplateParser.RULE_varInitExpr);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 165;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.VAR_REF:
			case AxeTemplateParser.FORMULA:
			case AxeTemplateParser.PERCENT:
			case AxeTemplateParser.DEC:
			case AxeTemplateParser.INT:
				{
				this.state = 163;
				_localctx._nval = this.numVarExpr();
				}
				break;
			case AxeTemplateParser.DQ_STRING:
			case AxeTemplateParser.SQ_STRING:
			case AxeTemplateParser.TICK_STRING:
				{
				this.state = 164;
				_localctx._sval = this.strVarExpr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public roleVarItem(): RoleVarItemContext {
		let _localctx: RoleVarItemContext = new RoleVarItemContext(this._ctx, this.state);
		this.enterRule(_localctx, 26, AxeTemplateParser.RULE_roleVarItem);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 167;
			_localctx._alias = this.roleAlias();
			this.state = 168;
			this.match(AxeTemplateParser.T__15);
			this.state = 169;
			_localctx._role = this.roleId();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public includeItem(): IncludeItemContext {
		let _localctx: IncludeItemContext = new IncludeItemContext(this._ctx, this.state);
		this.enterRule(_localctx, 28, AxeTemplateParser.RULE_includeItem);
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 171;
			this.match(AxeTemplateParser.T__17);
			this.state = 172;
			_localctx._tid = this.quotedStr();
			this.state = 181;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 13, this._ctx) ) {
			case 1:
				{
				this.state = 173;
				this.localScope();
				this.state = 178;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 12, this._ctx);
				while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
					if (_alt === 1) {
						{
						{
						this.state = 174;
						this.match(AxeTemplateParser.T__18);
						this.state = 175;
						this.localScope();
						}
						}
					}
					this.state = 180;
					this._errHandler.sync(this);
					_alt = this.interpreter.adaptivePredict(this._input, 12, this._ctx);
				}
				}
				break;
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public localScope(): LocalScopeContext {
		let _localctx: LocalScopeContext = new LocalScopeContext(this._ctx, this.state);
		this.enterRule(_localctx, 30, AxeTemplateParser.RULE_localScope);
		try {
			this.state = 185;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 14, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 183;
				_localctx._role = this.roleLocalScope();
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 184;
				_localctx._val = this.varLocalScope();
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public roleLocalScope(): RoleLocalScopeContext {
		let _localctx: RoleLocalScopeContext = new RoleLocalScopeContext(this._ctx, this.state);
		this.enterRule(_localctx, 32, AxeTemplateParser.RULE_roleLocalScope);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 187;
			_localctx._inner = this.roleId();
			this.state = 188;
			this.match(AxeTemplateParser.T__19);
			this.state = 189;
			_localctx._outer = this.roleId();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public varLocalScope(): VarLocalScopeContext {
		let _localctx: VarLocalScopeContext = new VarLocalScopeContext(this._ctx, this.state);
		this.enterRule(_localctx, 34, AxeTemplateParser.RULE_varLocalScope);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 191;
			_localctx._name = this.varName();
			this.state = 192;
			this.match(AxeTemplateParser.T__19);
			this.state = 193;
			_localctx._init = this.varInitExpr();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public accountStmt(): AccountStmtContext {
		let _localctx: AccountStmtContext = new AccountStmtContext(this._ctx, this.state);
		this.enterRule(_localctx, 36, AxeTemplateParser.RULE_accountStmt);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 195;
			_localctx._role = this.templateRole();
			this.state = 196;
			_localctx._left = this.accountSetterList();
			this.state = 197;
			this.match(AxeTemplateParser.T__20);
			this.state = 198;
			_localctx._right = this.accountSetterList();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public accountSetterList(): AccountSetterListContext {
		let _localctx: AccountSetterListContext = new AccountSetterListContext(this._ctx, this.state);
		this.enterRule(_localctx, 38, AxeTemplateParser.RULE_accountSetterList);
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 200;
			this.accountSetter();
			this.state = 205;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input, 15, this._ctx);
			while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
				if (_alt === 1) {
					{
					{
					this.state = 201;
					this.match(AxeTemplateParser.T__18);
					this.state = 202;
					this.accountSetter();
					}
					}
				}
				this.state = 207;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 15, this._ctx);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public accountSetter(): AccountSetterContext {
		let _localctx: AccountSetterContext = new AccountSetterContext(this._ctx, this.state);
		this.enterRule(_localctx, 40, AxeTemplateParser.RULE_accountSetter);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 208;
			_localctx._accId = this.accountId();
			this.state = 210;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 16, this._ctx) ) {
			case 1:
				{
				this.state = 209;
				_localctx._title = this.quotedStr();
				}
				break;
			}
			this.state = 213;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 17, this._ctx) ) {
			case 1:
				{
				this.state = 212;
				_localctx._val = this.value();
				}
				break;
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public accountId(): AccountIdContext {
		let _localctx: AccountIdContext = new AccountIdContext(this._ctx, this.state);
		this.enterRule(_localctx, 42, AxeTemplateParser.RULE_accountId);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 215;
			_localctx._number = this.accountNumber();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public commodityStmt(): CommodityStmtContext {
		let _localctx: CommodityStmtContext = new CommodityStmtContext(this._ctx, this.state);
		this.enterRule(_localctx, 44, AxeTemplateParser.RULE_commodityStmt);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 217;
			_localctx._asset = this.commodityStmtAsset();
			this.state = 218;
			_localctx._adj = this.assetAdjustment();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public commodityStmtAsset(): CommodityStmtAssetContext {
		let _localctx: CommodityStmtAssetContext = new CommodityStmtAssetContext(this._ctx, this.state);
		this.enterRule(_localctx, 46, AxeTemplateParser.RULE_commodityStmtAsset);
		let _la: number;
		try {
			this.state = 258;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.SWC:
				this.enterOuterAlt(_localctx, 1);
				{
				{
				this.state = 220;
				_localctx._swc = this.match(AxeTemplateParser.SWC);
				this.state = 222;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (AxeTemplateParser.VAR_REF - 32)) | (1 << (AxeTemplateParser.FORMULA - 32)) | (1 << (AxeTemplateParser.DEC - 32)) | (1 << (AxeTemplateParser.INT - 32)))) !== 0)) {
					{
					this.state = 221;
					_localctx._swcQty = this.qty();
					}
				}

				}
				}
				break;
			case AxeTemplateParser.BRC:
				this.enterOuterAlt(_localctx, 2);
				{
				{
				this.state = 224;
				_localctx._brc = this.match(AxeTemplateParser.BRC);
				this.state = 227;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === AxeTemplateParser.T__21) {
					{
					this.state = 225;
					this.match(AxeTemplateParser.T__21);
					this.state = 226;
					_localctx._brcUnit = this.unit();
					}
				}

				this.state = 230;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (AxeTemplateParser.VAR_REF - 32)) | (1 << (AxeTemplateParser.FORMULA - 32)) | (1 << (AxeTemplateParser.DEC - 32)) | (1 << (AxeTemplateParser.INT - 32)))) !== 0)) {
					{
					this.state = 229;
					_localctx._brcQty = this.qty();
					}
				}

				}
				}
				break;
			case AxeTemplateParser.FIAT:
				this.enterOuterAlt(_localctx, 3);
				{
				{
				this.state = 232;
				_localctx._fiat = this.match(AxeTemplateParser.FIAT);
				this.state = 234;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 21, this._ctx) ) {
				case 1:
					{
					this.state = 233;
					_localctx._fiatQty = this.qty();
					}
					break;
				}
				this.state = 237;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (AxeTemplateParser.VAR_REF - 32)) | (1 << (AxeTemplateParser.EXT_ID - 32)) | (1 << (AxeTemplateParser.ID - 32)) | (1 << (AxeTemplateParser.DQ_STRING - 32)) | (1 << (AxeTemplateParser.SQ_STRING - 32)) | (1 << (AxeTemplateParser.TICK_STRING - 32)))) !== 0)) {
					{
					this.state = 236;
					_localctx._fiatUnit = this.unit();
					}
				}

				}
				}
				break;
			case AxeTemplateParser.CRYPTO:
				this.enterOuterAlt(_localctx, 4);
				{
				{
				this.state = 239;
				_localctx._crypto = this.match(AxeTemplateParser.CRYPTO);
				this.state = 241;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 23, this._ctx) ) {
				case 1:
					{
					this.state = 240;
					_localctx._cryptoQty = this.qty();
					}
					break;
				}
				this.state = 244;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (AxeTemplateParser.VAR_REF - 32)) | (1 << (AxeTemplateParser.EXT_ID - 32)) | (1 << (AxeTemplateParser.ID - 32)) | (1 << (AxeTemplateParser.DQ_STRING - 32)) | (1 << (AxeTemplateParser.SQ_STRING - 32)) | (1 << (AxeTemplateParser.TICK_STRING - 32)))) !== 0)) {
					{
					this.state = 243;
					_localctx._cryptoUnit = this.unit();
					}
				}

				}
				}
				break;
			case AxeTemplateParser.VIRTUAL:
				this.enterOuterAlt(_localctx, 5);
				{
				{
				this.state = 246;
				_localctx._virtual = this.match(AxeTemplateParser.VIRTUAL);
				this.state = 248;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 25, this._ctx) ) {
				case 1:
					{
					this.state = 247;
					_localctx._virtualQty = this.qty();
					}
					break;
				}
				this.state = 251;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (AxeTemplateParser.VAR_REF - 32)) | (1 << (AxeTemplateParser.EXT_ID - 32)) | (1 << (AxeTemplateParser.ID - 32)) | (1 << (AxeTemplateParser.DQ_STRING - 32)) | (1 << (AxeTemplateParser.SQ_STRING - 32)) | (1 << (AxeTemplateParser.TICK_STRING - 32)))) !== 0)) {
					{
					this.state = 250;
					_localctx._virtualUnit = this.unit();
					}
				}

				}
				}
				break;
			case AxeTemplateParser.COMMODITY:
				this.enterOuterAlt(_localctx, 6);
				{
				{
				this.state = 253;
				this.match(AxeTemplateParser.COMMODITY);
				this.state = 254;
				_localctx._typeId = this.assetTypeId();
				this.state = 255;
				_localctx._macQty = this.qty();
				this.state = 256;
				_localctx._macUnit = this.unit();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public discreteStmt(): DiscreteStmtContext {
		let _localctx: DiscreteStmtContext = new DiscreteStmtContext(this._ctx, this.state);
		this.enterRule(_localctx, 48, AxeTemplateParser.RULE_discreteStmt);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 260;
			this.match(AxeTemplateParser.DISCRETE);
			this.state = 261;
			_localctx._asset = this.discreteStmtAsset();
			this.state = 262;
			_localctx._adj = this.assetAdjustment();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public discreteStmtAsset(): DiscreteStmtAssetContext {
		let _localctx: DiscreteStmtAssetContext = new DiscreteStmtAssetContext(this._ctx, this.state);
		this.enterRule(_localctx, 50, AxeTemplateParser.RULE_discreteStmtAsset);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 264;
			_localctx._typeId = this.assetTypeId();
			this.state = 265;
			_localctx._macAssetId = this.assetId();
			this.state = 266;
			_localctx._macShare = this.share();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public assetAdjustment(): AssetAdjustmentContext {
		let _localctx: AssetAdjustmentContext = new AssetAdjustmentContext(this._ctx, this.state);
		this.enterRule(_localctx, 52, AxeTemplateParser.RULE_assetAdjustment);
		try {
			this.state = 277;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 29, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				{
				this.state = 268;
				_localctx._role = this.templateRole();
				this.state = 271;
				this._errHandler.sync(this);
				switch (this._input.LA(1)) {
				case AxeTemplateParser.T__22:
					{
					this.state = 269;
					_localctx._incr = this.match(AxeTemplateParser.T__22);
					}
					break;
				case AxeTemplateParser.T__23:
					{
					this.state = 270;
					_localctx._decr = this.match(AxeTemplateParser.T__23);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				{
				this.state = 273;
				_localctx._from = this.templateRole();
				this.state = 274;
				this.match(AxeTemplateParser.T__24);
				this.state = 275;
				_localctx._to = this.templateRole();
				}
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public assetId(): AssetIdContext {
		let _localctx: AssetIdContext = new AssetIdContext(this._ctx, this.state);
		this.enterRule(_localctx, 54, AxeTemplateParser.RULE_assetId);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 279;
			_localctx._rf = this.match(AxeTemplateParser.VAR_REF);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public value(): ValueContext {
		let _localctx: ValueContext = new ValueContext(this._ctx, this.state);
		this.enterRule(_localctx, 56, AxeTemplateParser.RULE_value);
		try {
			this.state = 284;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.T__25:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 281;
				_localctx._remainder = this.match(AxeTemplateParser.T__25);
				}
				break;
			case AxeTemplateParser.DEC:
			case AxeTemplateParser.INT:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 282;
				_localctx._num = this.extNum();
				}
				break;
			case AxeTemplateParser.VAR_REF:
			case AxeTemplateParser.FORMULA:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 283;
				_localctx._form = this.expr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public unit(): UnitContext {
		let _localctx: UnitContext = new UnitContext(this._ctx, this.state);
		this.enterRule(_localctx, 58, AxeTemplateParser.RULE_unit);
		try {
			this.state = 288;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.EXT_ID:
			case AxeTemplateParser.ID:
			case AxeTemplateParser.DQ_STRING:
			case AxeTemplateParser.SQ_STRING:
			case AxeTemplateParser.TICK_STRING:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 286;
				_localctx._id = this.extId();
				}
				break;
			case AxeTemplateParser.VAR_REF:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 287;
				_localctx._rf = this.match(AxeTemplateParser.VAR_REF);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public qty(): QtyContext {
		let _localctx: QtyContext = new QtyContext(this._ctx, this.state);
		this.enterRule(_localctx, 60, AxeTemplateParser.RULE_qty);
		try {
			this.state = 292;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.DEC:
			case AxeTemplateParser.INT:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 290;
				_localctx._lit = this.extNum();
				}
				break;
			case AxeTemplateParser.VAR_REF:
			case AxeTemplateParser.FORMULA:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 291;
				_localctx._form = this.expr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public share(): ShareContext {
		let _localctx: ShareContext = new ShareContext(this._ctx, this.state);
		this.enterRule(_localctx, 62, AxeTemplateParser.RULE_share);
		try {
			this.state = 296;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.PERCENT:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 294;
				_localctx._lit = this.match(AxeTemplateParser.PERCENT);
				}
				break;
			case AxeTemplateParser.VAR_REF:
			case AxeTemplateParser.FORMULA:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 295;
				_localctx._form = this.expr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public numVarExpr(): NumVarExprContext {
		let _localctx: NumVarExprContext = new NumVarExprContext(this._ctx, this.state);
		this.enterRule(_localctx, 64, AxeTemplateParser.RULE_numVarExpr);
		try {
			this.state = 301;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.VAR_REF:
			case AxeTemplateParser.FORMULA:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 298;
				_localctx._form = this.expr();
				}
				break;
			case AxeTemplateParser.PERCENT:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 299;
				_localctx._perc = this.match(AxeTemplateParser.PERCENT);
				}
				break;
			case AxeTemplateParser.DEC:
			case AxeTemplateParser.INT:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 300;
				_localctx._noPerc = this.extNum();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public strVarExpr(): StrVarExprContext {
		let _localctx: StrVarExprContext = new StrVarExprContext(this._ctx, this.state);
		this.enterRule(_localctx, 66, AxeTemplateParser.RULE_strVarExpr);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 303;
			_localctx._lit = this.quotedStr();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public expr(): ExprContext {
		let _localctx: ExprContext = new ExprContext(this._ctx, this.state);
		this.enterRule(_localctx, 68, AxeTemplateParser.RULE_expr);
		try {
			this.state = 307;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.FORMULA:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 305;
				_localctx._form = this.match(AxeTemplateParser.FORMULA);
				}
				break;
			case AxeTemplateParser.VAR_REF:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 306;
				_localctx._ref = this.match(AxeTemplateParser.VAR_REF);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public templateRole(): TemplateRoleContext {
		let _localctx: TemplateRoleContext = new TemplateRoleContext(this._ctx, this.state);
		this.enterRule(_localctx, 70, AxeTemplateParser.RULE_templateRole);
		try {
			this.state = 311;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.ROLE_PREFIX:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 309;
				this.roleId();
				}
				break;
			case AxeTemplateParser.ROLE_ALIAS_PREFIX:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 310;
				this.roleAlias();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public roleAlias(): RoleAliasContext {
		let _localctx: RoleAliasContext = new RoleAliasContext(this._ctx, this.state);
		this.enterRule(_localctx, 72, AxeTemplateParser.RULE_roleAlias);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 313;
			this.match(AxeTemplateParser.ROLE_ALIAS_PREFIX);
			this.state = 314;
			_localctx._id = this.extId();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public roleId(): RoleIdContext {
		let _localctx: RoleIdContext = new RoleIdContext(this._ctx, this.state);
		this.enterRule(_localctx, 74, AxeTemplateParser.RULE_roleId);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 316;
			this.match(AxeTemplateParser.ROLE_PREFIX);
			this.state = 317;
			_localctx._id = this.extId();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public accountNumber(): AccountNumberContext {
		let _localctx: AccountNumberContext = new AccountNumberContext(this._ctx, this.state);
		this.enterRule(_localctx, 76, AxeTemplateParser.RULE_accountNumber);
		let _la: number;
		try {
			this.state = 328;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 37, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 319;
				_localctx._num = this.match(AxeTemplateParser.INT);
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 320;
				_localctx._id = this.match(AxeTemplateParser.ID);
				}
				break;

			case 3:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 321;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.COMMODITY)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 4:
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 322;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.DISCRETE)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 5:
				this.enterOuterAlt(_localctx, 5);
				{
				this.state = 323;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.SWC)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 6:
				this.enterOuterAlt(_localctx, 6);
				{
				this.state = 324;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.BRC)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 7:
				this.enterOuterAlt(_localctx, 7);
				{
				this.state = 325;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.FIAT)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 8:
				this.enterOuterAlt(_localctx, 8);
				{
				this.state = 326;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.CRYPTO)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 9:
				this.enterOuterAlt(_localctx, 9);
				{
				this.state = 327;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.VIRTUAL)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public extNum(): ExtNumContext {
		let _localctx: ExtNumContext = new ExtNumContext(this._ctx, this.state);
		this.enterRule(_localctx, 78, AxeTemplateParser.RULE_extNum);
		try {
			this.state = 332;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.DEC:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 330;
				_localctx._dec = this.match(AxeTemplateParser.DEC);
				}
				break;
			case AxeTemplateParser.INT:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 331;
				_localctx._int = this.match(AxeTemplateParser.INT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public extId(): ExtIdContext {
		let _localctx: ExtIdContext = new ExtIdContext(this._ctx, this.state);
		this.enterRule(_localctx, 80, AxeTemplateParser.RULE_extId);
		try {
			this.state = 337;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.DQ_STRING:
			case AxeTemplateParser.SQ_STRING:
			case AxeTemplateParser.TICK_STRING:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 334;
				_localctx._qs = this.quotedStr();
				}
				break;
			case AxeTemplateParser.ID:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 335;
				_localctx._id = this.match(AxeTemplateParser.ID);
				}
				break;
			case AxeTemplateParser.EXT_ID:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 336;
				_localctx._ext = this.match(AxeTemplateParser.EXT_ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public quotedStr(): QuotedStrContext {
		let _localctx: QuotedStrContext = new QuotedStrContext(this._ctx, this.state);
		this.enterRule(_localctx, 82, AxeTemplateParser.RULE_quotedStr);
		try {
			this.state = 342;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case AxeTemplateParser.DQ_STRING:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 339;
				_localctx._dq = this.match(AxeTemplateParser.DQ_STRING);
				}
				break;
			case AxeTemplateParser.SQ_STRING:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 340;
				_localctx._sq = this.match(AxeTemplateParser.SQ_STRING);
				}
				break;
			case AxeTemplateParser.TICK_STRING:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 341;
				_localctx._ts = this.match(AxeTemplateParser.TICK_STRING);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public assetTypeId(): AssetTypeIdContext {
		let _localctx: AssetTypeIdContext = new AssetTypeIdContext(this._ctx, this.state);
		this.enterRule(_localctx, 84, AxeTemplateParser.RULE_assetTypeId);
		let _la: number;
		try {
			this.state = 353;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 41, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 344;
				this.match(AxeTemplateParser.ID);
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 345;
				this.match(AxeTemplateParser.EXT_ID);
				}
				break;

			case 3:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 346;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.COMMODITY)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 4:
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 347;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.DISCRETE)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 5:
				this.enterOuterAlt(_localctx, 5);
				{
				this.state = 348;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.SWC)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 6:
				this.enterOuterAlt(_localctx, 6);
				{
				this.state = 349;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.BRC)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 7:
				this.enterOuterAlt(_localctx, 7);
				{
				this.state = 350;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.FIAT)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 8:
				this.enterOuterAlt(_localctx, 8);
				{
				this.state = 351;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.CRYPTO)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;

			case 9:
				this.enterOuterAlt(_localctx, 9);
				{
				this.state = 352;
				_la = this._input.LA(1);
				if (_la <= 0 || (_la === AxeTemplateParser.VIRTUAL)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public signature(): SignatureContext {
		let _localctx: SignatureContext = new SignatureContext(this._ctx, this.state);
		this.enterRule(_localctx, 86, AxeTemplateParser.RULE_signature);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 355;
			_localctx._val = this.quotedStr();
			this.state = 366;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === AxeTemplateParser.T__26) {
				{
				this.state = 356;
				this.match(AxeTemplateParser.T__26);
				this.state = 357;
				_localctx._signer = this.extId();
				this.state = 364;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === AxeTemplateParser.T__27) {
					{
					this.state = 358;
					this.match(AxeTemplateParser.T__27);
					this.state = 359;
					_localctx._mandator = this.extId();
					this.state = 362;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
					if (_la === AxeTemplateParser.T__28) {
						{
						this.state = 360;
						this.match(AxeTemplateParser.T__28);
						this.state = 361;
						_localctx._mandate = this.extId();
						}
					}

					}
				}

				}
			}

			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}

	public static readonly _serializedATN: string =
		"\x03\uC91D\uCABA\u058D\uAFBA\u4F53\u0607\uEA8B\uC241\x037\u0173\x04\x02" +
		"\t\x02\x04\x03\t\x03\x04\x04\t\x04\x04\x05\t\x05\x04\x06\t\x06\x04\x07" +
		"\t\x07\x04\b\t\b\x04\t\t\t\x04\n\t\n\x04\v\t\v\x04\f\t\f\x04\r\t\r\x04" +
		"\x0E\t\x0E\x04\x0F\t\x0F\x04\x10\t\x10\x04\x11\t\x11\x04\x12\t\x12\x04" +
		"\x13\t\x13\x04\x14\t\x14\x04\x15\t\x15\x04\x16\t\x16\x04\x17\t\x17\x04" +
		"\x18\t\x18\x04\x19\t\x19\x04\x1A\t\x1A\x04\x1B\t\x1B\x04\x1C\t\x1C\x04" +
		"\x1D\t\x1D\x04\x1E\t\x1E\x04\x1F\t\x1F\x04 \t \x04!\t!\x04\"\t\"\x04#" +
		"\t#\x04$\t$\x04%\t%\x04&\t&\x04\'\t\'\x04(\t(\x04)\t)\x04*\t*\x04+\t+" +
		"\x04,\t,\x04-\t-\x03\x02\x03\x02\x03\x02\x06\x02^\n\x02\r\x02\x0E\x02" +
		"_\x03\x03\x03\x03\x05\x03d\n\x03\x03\x03\x03\x03\x03\x03\x03\x04\x03\x04" +
		"\x03\x04\x05\x04l\n\x04\x07\x04n\n\x04\f\x04\x0E\x04q\v\x04\x03\x05\x03" +
		"\x05\x05\x05u\n\x05\x03\x06\x03\x06\x03\x06\x05\x06z\n\x06\x03\x07\x03" +
		"\x07\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x05\b\x84\n\b\x03\t\x03\t\x03" +
		"\t\x03\t\x03\t\x03\t\x03\t\x03\t\x05\t\x8E\n\t\x03\n\x06\n\x91\n\n\r\n" +
		"\x0E\n\x92\x03\v\x03\v\x03\v\x05\v\x98\n\v\x03\v\x03\v\x03\f\x03\f\x03" +
		"\f\x03\f\x03\r\x03\r\x03\r\x03\r\x05\r\xA4\n\r\x03\x0E\x03\x0E\x05\x0E" +
		"\xA8\n\x0E\x03\x0F\x03\x0F\x03\x0F\x03\x0F\x03\x10\x03\x10\x03\x10\x03" +
		"\x10\x03\x10\x07\x10\xB3\n\x10\f\x10\x0E\x10\xB6\v\x10\x05\x10\xB8\n\x10" +
		"\x03\x11\x03\x11\x05\x11\xBC\n\x11\x03\x12\x03\x12\x03\x12\x03\x12\x03" +
		"\x13\x03\x13\x03\x13\x03\x13\x03\x14\x03\x14\x03\x14\x03\x14\x03\x14\x03" +
		"\x15\x03\x15\x03\x15\x07\x15\xCE\n\x15\f\x15\x0E\x15\xD1\v\x15\x03\x16" +
		"\x03\x16\x05\x16\xD5\n\x16\x03\x16\x05\x16\xD8\n\x16\x03\x17\x03\x17\x03" +
		"\x18\x03\x18\x03\x18\x03\x19\x03\x19\x05\x19\xE1\n\x19\x03\x19\x03\x19" +
		"\x03\x19\x05\x19\xE6\n\x19\x03\x19\x05\x19\xE9\n\x19\x03\x19\x03\x19\x05" +
		"\x19\xED\n\x19\x03\x19\x05\x19\xF0\n\x19\x03\x19\x03\x19\x05\x19\xF4\n" +
		"\x19\x03\x19\x05\x19\xF7\n\x19\x03\x19\x03\x19\x05\x19\xFB\n\x19\x03\x19" +
		"\x05\x19\xFE\n\x19\x03\x19\x03\x19\x03\x19\x03\x19\x03\x19\x05\x19\u0105" +
		"\n\x19\x03\x1A\x03\x1A\x03\x1A\x03\x1A\x03\x1B\x03\x1B\x03\x1B\x03\x1B" +
		"\x03\x1C\x03\x1C\x03\x1C\x05\x1C\u0112\n\x1C\x03\x1C\x03\x1C\x03\x1C\x03" +
		"\x1C\x05\x1C\u0118\n\x1C\x03\x1D\x03\x1D\x03\x1E\x03\x1E\x03\x1E\x05\x1E" +
		"\u011F\n\x1E\x03\x1F\x03\x1F\x05\x1F\u0123\n\x1F\x03 \x03 \x05 \u0127" +
		"\n \x03!\x03!\x05!\u012B\n!\x03\"\x03\"\x03\"\x05\"\u0130\n\"\x03#\x03" +
		"#\x03$\x03$\x05$\u0136\n$\x03%\x03%\x05%\u013A\n%\x03&\x03&\x03&\x03\'" +
		"\x03\'\x03\'\x03(\x03(\x03(\x03(\x03(\x03(\x03(\x03(\x03(\x05(\u014B\n" +
		"(\x03)\x03)\x05)\u014F\n)\x03*\x03*\x03*\x05*\u0154\n*\x03+\x03+\x03+" +
		"\x05+\u0159\n+\x03,\x03,\x03,\x03,\x03,\x03,\x03,\x03,\x03,\x05,\u0164" +
		"\n,\x03-\x03-\x03-\x03-\x03-\x03-\x03-\x05-\u016D\n-\x05-\u016F\n-\x05" +
		"-\u0171\n-\x03-\x02\x02\x02.\x02\x02\x04\x02\x06\x02\b\x02\n\x02\f\x02" +
		"\x0E\x02\x10\x02\x12\x02\x14\x02\x16\x02\x18\x02\x1A\x02\x1C\x02\x1E\x02" +
		" \x02\"\x02$\x02&\x02(\x02*\x02,\x02.\x020\x022\x024\x026\x028\x02:\x02" +
		"<\x02>\x02@\x02B\x02D\x02F\x02H\x02J\x02L\x02N\x02P\x02R\x02T\x02V\x02" +
		"X\x02\x02\f\x04\x02\x06\t22\x03\x02\x13\x13\x03\x02\x06\x06\x03\x02%%" +
		"\x03\x02&&\x03\x02\'\'\x03\x02((\x03\x02))\x03\x02**\x03\x02++\x02\u0195" +
		"\x02Z\x03\x02\x02\x02\x04c\x03\x02\x02\x02\x06o\x03\x02\x02\x02\br\x03" +
		"\x02\x02\x02\nv\x03\x02\x02\x02\f{\x03\x02\x02\x02\x0E\x83\x03\x02\x02" +
		"\x02\x10\x8D\x03\x02\x02\x02\x12\x90\x03\x02\x02\x02\x14\x97\x03\x02\x02" +
		"\x02\x16\x9B\x03\x02\x02\x02\x18\xA3\x03\x02\x02\x02\x1A\xA7\x03\x02\x02" +
		"\x02\x1C\xA9\x03\x02\x02\x02\x1E\xAD\x03\x02\x02\x02 \xBB\x03\x02\x02" +
		"\x02\"\xBD\x03\x02\x02\x02$\xC1\x03\x02\x02\x02&\xC5\x03\x02\x02\x02(" +
		"\xCA\x03\x02\x02\x02*\xD2\x03\x02\x02\x02,\xD9\x03\x02\x02\x02.\xDB\x03" +
		"\x02\x02\x020\u0104\x03\x02\x02\x022\u0106\x03\x02\x02\x024\u010A\x03" +
		"\x02\x02\x026\u0117\x03\x02\x02\x028\u0119\x03\x02\x02\x02:\u011E\x03" +
		"\x02\x02\x02<\u0122\x03\x02\x02\x02>\u0126\x03\x02\x02\x02@\u012A\x03" +
		"\x02\x02\x02B\u012F\x03\x02\x02\x02D\u0131\x03\x02\x02\x02F\u0135\x03" +
		"\x02\x02\x02H\u0139\x03\x02\x02\x02J\u013B\x03\x02\x02\x02L\u013E\x03" +
		"\x02\x02\x02N\u014A\x03\x02\x02\x02P\u014E\x03\x02\x02\x02R\u0153\x03" +
		"\x02\x02\x02T\u0158\x03\x02\x02\x02V\u0163\x03\x02\x02\x02X\u0165\x03" +
		"\x02\x02\x02Z[\x05\x04\x03\x02[]\x05\x06\x04\x02\\^\x05\x10\t\x02]\\\x03" +
		"\x02\x02\x02^_\x03\x02\x02\x02_]\x03\x02\x02\x02_`\x03\x02\x02\x02`\x03" +
		"\x03\x02\x02\x02ab\x07\x03\x02\x02bd\x05T+\x02ca\x03\x02\x02\x02cd\x03" +
		"\x02\x02\x02de\x03\x02\x02\x02ef\x07\x04\x02\x02fg\x05T+\x02g\x05\x03" +
		"\x02\x02\x02hk\x07\x05\x02\x02il\x05\b\x05\x02jl\x05\n\x06\x02ki\x03\x02" +
		"\x02\x02kj\x03\x02\x02\x02ln\x03\x02\x02\x02mh\x03\x02\x02\x02nq\x03\x02" +
		"\x02\x02om\x03\x02\x02\x02op\x03\x02\x02\x02p\x07\x03\x02\x02\x02qo\x03" +
		"\x02\x02\x02rt\x05L\'\x02su\x05T+\x02ts\x03\x02\x02\x02tu\x03\x02\x02" +
		"\x02u\t\x03\x02\x02\x02vw\x05\f\x07\x02wy\x05\x0E\b\x02xz\x05T+\x02yx" +
		"\x03\x02\x02\x02yz\x03\x02\x02\x02z\v\x03\x02\x02\x02{|\t\x02\x02\x02" +
		"|\r\x03\x02\x02\x02}\x84\x07\n\x02\x02~\x84\x07\v\x02\x02\x7F\x84\x07" +
		"\f\x02\x02\x80\x84\x07\r\x02\x02\x81\x84\x07\x0E\x02\x02\x82\x84\x07\x0F" +
		"\x02\x02\x83}\x03\x02\x02\x02\x83~\x03\x02\x02\x02\x83\x7F\x03\x02\x02" +
		"\x02\x83\x80\x03\x02\x02\x02\x83\x81\x03\x02\x02\x02\x83\x82\x03\x02\x02" +
		"\x02\x84\x0F\x03\x02\x02\x02\x85\x8E\x05\x14\v\x02\x86\x8E\x05\x1C\x0F" +
		"\x02\x87\x8E\x05\x16\f\x02\x88\x8E\x05\x1E\x10\x02\x89\x8E\x05&\x14\x02" +
		"\x8A\x8E\x05.\x18\x02\x8B\x8E\x052\x1A\x02\x8C\x8E\x05\x12\n\x02\x8D\x85" +
		"\x03\x02\x02\x02\x8D\x86\x03\x02\x02\x02\x8D\x87\x03\x02\x02\x02\x8D\x88" +
		"\x03\x02\x02\x02\x8D\x89\x03\x02\x02\x02\x8D\x8A\x03\x02\x02\x02\x8D\x8B" +
		"\x03\x02\x02\x02\x8D\x8C\x03\x02\x02\x02\x8E\x11\x03\x02\x02\x02\x8F\x91" +
		"\x07$\x02\x02\x90\x8F\x03\x02\x02\x02\x91\x92\x03\x02\x02\x02\x92\x90" +
		"\x03\x02\x02\x02\x92\x93\x03\x02\x02\x02\x93\x13\x03\x02\x02\x02\x94\x95" +
		"\x07\x10\x02\x02\x95\x96\x070\x02\x02\x96\x98\x07\x11\x02\x02\x97\x94" +
		"\x03\x02\x02\x02\x97\x98\x03\x02\x02\x02\x98\x99\x03\x02\x02\x02\x99\x9A" +
		"\x05T+\x02\x9A\x15\x03\x02\x02\x02\x9B\x9C\x05\x18\r\x02\x9C\x9D\x07\x12" +
		"\x02\x02\x9D\x9E\x05\x1A\x0E\x02\x9E\x17\x03\x02\x02\x02\x9F\xA4\x072" +
		"\x02\x02\xA0\xA4\x07/\x02\x02\xA1\xA4\n\x03\x02\x02\xA2\xA4\n\x04\x02" +
		"\x02\xA3\x9F\x03\x02\x02\x02\xA3\xA0\x03\x02\x02\x02\xA3\xA1\x03\x02\x02" +
		"\x02\xA3\xA2\x03\x02\x02\x02\xA4\x19\x03\x02\x02\x02\xA5\xA8\x05B\"\x02" +
		"\xA6\xA8\x05D#\x02\xA7\xA5\x03\x02\x02\x02\xA7\xA6\x03\x02\x02\x02\xA8" +
		"\x1B\x03\x02\x02\x02\xA9\xAA\x05J&\x02\xAA\xAB\x07\x12\x02\x02\xAB\xAC" +
		"\x05L\'\x02\xAC\x1D\x03\x02\x02\x02\xAD\xAE\x07\x14\x02\x02\xAE\xB7\x05" +
		"T+\x02\xAF\xB4\x05 \x11\x02\xB0\xB1\x07\x15\x02\x02\xB1\xB3\x05 \x11\x02" +
		"\xB2\xB0\x03\x02\x02\x02\xB3\xB6\x03\x02\x02\x02\xB4\xB2\x03\x02\x02\x02" +
		"\xB4\xB5\x03\x02\x02\x02\xB5\xB8\x03\x02\x02\x02\xB6\xB4\x03\x02\x02\x02" +
		"\xB7\xAF\x03\x02\x02\x02\xB7\xB8\x03\x02\x02\x02\xB8\x1F\x03\x02\x02\x02" +
		"\xB9\xBC\x05\"\x12\x02\xBA\xBC\x05$\x13\x02\xBB\xB9\x03\x02\x02\x02\xBB" +
		"\xBA\x03\x02\x02\x02\xBC!\x03\x02\x02\x02\xBD\xBE\x05L\'\x02\xBE\xBF\x07" +
		"\x16\x02\x02\xBF\xC0\x05L\'\x02\xC0#\x03\x02\x02\x02\xC1\xC2\x05\x18\r" +
		"\x02\xC2\xC3\x07\x16\x02\x02\xC3\xC4\x05\x1A\x0E\x02\xC4%\x03\x02\x02" +
		"\x02\xC5\xC6\x05H%\x02\xC6\xC7\x05(\x15\x02\xC7\xC8\x07\x17\x02\x02\xC8" +
		"\xC9\x05(\x15\x02\xC9\'\x03\x02\x02\x02\xCA\xCF\x05*\x16\x02\xCB\xCC\x07" +
		"\x15\x02\x02\xCC\xCE\x05*\x16\x02\xCD\xCB\x03\x02\x02\x02\xCE\xD1\x03" +
		"\x02\x02\x02\xCF\xCD\x03\x02\x02\x02\xCF\xD0\x03\x02\x02\x02\xD0)\x03" +
		"\x02\x02\x02\xD1\xCF\x03\x02\x02\x02\xD2\xD4\x05,\x17\x02\xD3\xD5\x05" +
		"T+\x02\xD4\xD3\x03\x02\x02\x02\xD4\xD5\x03\x02\x02\x02\xD5\xD7\x03\x02" +
		"\x02\x02\xD6\xD8\x05:\x1E\x02\xD7\xD6\x03\x02\x02\x02\xD7\xD8\x03\x02" +
		"\x02\x02\xD8+\x03\x02\x02\x02\xD9\xDA\x05N(\x02\xDA-\x03\x02\x02\x02\xDB" +
		"\xDC\x050\x19\x02\xDC\xDD\x056\x1C\x02\xDD/\x03\x02\x02\x02\xDE\xE0\x07" +
		"\'\x02\x02\xDF\xE1\x05> \x02\xE0\xDF\x03\x02\x02\x02\xE0\xE1\x03\x02\x02" +
		"\x02\xE1\u0105\x03\x02\x02\x02\xE2\xE5\x07(\x02\x02\xE3\xE4\x07\x18\x02" +
		"\x02\xE4\xE6\x05<\x1F\x02\xE5\xE3\x03\x02\x02\x02\xE5\xE6\x03\x02\x02" +
		"\x02\xE6\xE8\x03\x02\x02\x02\xE7\xE9\x05> \x02\xE8\xE7\x03\x02\x02\x02" +
		"\xE8\xE9\x03\x02\x02\x02\xE9\u0105\x03\x02\x02\x02\xEA\xEC\x07)\x02\x02" +
		"\xEB\xED\x05> \x02\xEC\xEB\x03\x02\x02\x02\xEC\xED\x03\x02\x02\x02\xED" +
		"\xEF\x03\x02\x02\x02\xEE\xF0\x05<\x1F\x02\xEF\xEE\x03\x02\x02\x02\xEF" +
		"\xF0\x03\x02\x02\x02\xF0\u0105\x03\x02\x02\x02\xF1\xF3\x07*\x02\x02\xF2" +
		"\xF4\x05> \x02\xF3\xF2\x03\x02\x02\x02\xF3\xF4\x03\x02\x02\x02\xF4\xF6" +
		"\x03\x02\x02\x02\xF5\xF7\x05<\x1F\x02\xF6\xF5\x03\x02\x02\x02\xF6\xF7" +
		"\x03\x02\x02\x02\xF7\u0105\x03\x02\x02\x02\xF8\xFA\x07+\x02\x02\xF9\xFB" +
		"\x05> \x02\xFA\xF9\x03\x02\x02\x02\xFA\xFB\x03\x02\x02\x02\xFB\xFD\x03" +
		"\x02\x02\x02\xFC\xFE\x05<\x1F\x02\xFD\xFC\x03\x02\x02\x02\xFD\xFE\x03" +
		"\x02\x02\x02\xFE\u0105\x03\x02\x02\x02\xFF\u0100\x07%\x02\x02\u0100\u0101" +
		"\x05V,\x02\u0101\u0102\x05> \x02\u0102\u0103\x05<\x1F\x02\u0103\u0105" +
		"\x03\x02\x02\x02\u0104\xDE\x03\x02\x02\x02\u0104\xE2\x03\x02\x02\x02\u0104" +
		"\xEA\x03\x02\x02\x02\u0104\xF1\x03\x02\x02\x02\u0104\xF8\x03\x02\x02\x02" +
		"\u0104\xFF\x03\x02\x02\x02\u01051\x03\x02\x02\x02\u0106\u0107\x07&\x02" +
		"\x02\u0107\u0108\x054\x1B\x02\u0108\u0109\x056\x1C\x02\u01093\x03\x02" +
		"\x02\x02\u010A\u010B\x05V,\x02\u010B\u010C\x058\x1D\x02\u010C\u010D\x05" +
		"@!\x02\u010D5\x03\x02\x02\x02\u010E\u0111\x05H%\x02\u010F\u0112\x07\x19" +
		"\x02\x02\u0110\u0112\x07\x1A\x02\x02\u0111\u010F\x03\x02\x02\x02\u0111" +
		"\u0110\x03\x02\x02\x02\u0112\u0118\x03\x02\x02\x02\u0113\u0114\x05H%\x02" +
		"\u0114\u0115\x07\x1B\x02\x02\u0115\u0116\x05H%\x02\u0116\u0118\x03\x02" +
		"\x02\x02\u0117\u010E\x03\x02\x02\x02\u0117\u0113\x03\x02\x02\x02\u0118" +
		"7\x03\x02\x02\x02\u0119\u011A\x07\"\x02\x02\u011A9\x03\x02\x02\x02\u011B" +
		"\u011F\x07\x1C\x02\x02\u011C\u011F\x05P)\x02\u011D\u011F\x05F$\x02\u011E" +
		"\u011B\x03\x02\x02\x02\u011E\u011C\x03\x02\x02\x02\u011E\u011D\x03\x02" +
		"\x02\x02\u011F;\x03\x02\x02\x02\u0120\u0123\x05R*\x02\u0121\u0123\x07" +
		"\"\x02\x02\u0122\u0120\x03\x02\x02\x02\u0122\u0121\x03\x02\x02\x02\u0123" +
		"=\x03\x02\x02\x02\u0124\u0127\x05P)\x02\u0125\u0127\x05F$\x02\u0126\u0124" +
		"\x03\x02\x02\x02\u0126\u0125\x03\x02\x02\x02\u0127?\x03\x02\x02\x02\u0128" +
		"\u012B\x07,\x02\x02\u0129\u012B\x05F$\x02\u012A\u0128\x03\x02\x02\x02" +
		"\u012A\u0129\x03\x02\x02\x02\u012BA\x03\x02\x02\x02\u012C\u0130\x05F$" +
		"\x02\u012D\u0130\x07,\x02\x02\u012E\u0130\x05P)\x02\u012F\u012C\x03\x02" +
		"\x02\x02\u012F\u012D\x03\x02\x02\x02\u012F\u012E\x03\x02\x02\x02\u0130" +
		"C\x03\x02\x02\x02\u0131\u0132\x05T+\x02\u0132E\x03\x02\x02\x02\u0133\u0136" +
		"\x07#\x02\x02\u0134\u0136\x07\"\x02\x02\u0135\u0133\x03\x02\x02\x02\u0135" +
		"\u0134\x03\x02\x02\x02\u0136G\x03\x02\x02\x02\u0137\u013A\x05L\'\x02\u0138" +
		"\u013A\x05J&\x02\u0139\u0137\x03\x02\x02\x02\u0139\u0138\x03\x02\x02\x02" +
		"\u013AI\x03\x02\x02\x02\u013B\u013C\x07 \x02\x02\u013C\u013D\x05R*\x02" +
		"\u013DK\x03\x02\x02\x02\u013E\u013F\x07!\x02\x02\u013F\u0140\x05R*\x02" +
		"\u0140M\x03\x02\x02\x02\u0141\u014B\x070\x02\x02\u0142\u014B\x072\x02" +
		"\x02\u0143\u014B\n\x05\x02\x02\u0144\u014B\n\x06\x02\x02\u0145\u014B\n" +
		"\x07\x02\x02\u0146\u014B\n\b\x02\x02\u0147\u014B\n\t\x02\x02\u0148\u014B" +
		"\n\n\x02\x02\u0149\u014B\n\v\x02\x02\u014A\u0141\x03\x02\x02\x02\u014A" +
		"\u0142\x03\x02\x02\x02\u014A\u0143\x03\x02\x02\x02\u014A\u0144\x03\x02" +
		"\x02\x02\u014A\u0145\x03\x02\x02\x02\u014A\u0146\x03\x02\x02\x02\u014A" +
		"\u0147\x03\x02\x02\x02\u014A\u0148\x03\x02\x02\x02\u014A\u0149\x03\x02" +
		"\x02\x02\u014BO\x03\x02\x02\x02\u014C\u014F\x07-\x02\x02\u014D\u014F\x07" +
		"0\x02\x02\u014E\u014C\x03\x02\x02\x02\u014E\u014D\x03\x02\x02\x02\u014F" +
		"Q\x03\x02\x02\x02\u0150\u0154\x05T+\x02\u0151\u0154\x072\x02\x02\u0152" +
		"\u0154\x07/\x02\x02\u0153\u0150\x03\x02\x02\x02\u0153\u0151\x03\x02\x02" +
		"\x02\u0153\u0152\x03\x02\x02\x02\u0154S\x03\x02\x02\x02\u0155\u0159\x07" +
		"3\x02\x02\u0156\u0159\x074\x02\x02\u0157\u0159\x075\x02\x02\u0158\u0155" +
		"\x03\x02\x02\x02\u0158\u0156\x03\x02\x02\x02\u0158\u0157\x03\x02\x02\x02" +
		"\u0159U\x03\x02\x02\x02\u015A\u0164\x072\x02\x02\u015B\u0164\x07/\x02" +
		"\x02\u015C\u0164\n\x05\x02\x02\u015D\u0164\n\x06\x02\x02\u015E\u0164\n" +
		"\x07\x02\x02\u015F\u0164\n\b\x02\x02\u0160\u0164\n\t\x02\x02\u0161\u0164" +
		"\n\n\x02\x02\u0162\u0164\n\v\x02\x02\u0163\u015A\x03\x02\x02\x02\u0163" +
		"\u015B\x03\x02\x02\x02\u0163\u015C\x03\x02\x02\x02\u0163\u015D\x03\x02" +
		"\x02\x02\u0163\u015E\x03\x02\x02\x02\u0163\u015F\x03\x02\x02\x02\u0163" +
		"\u0160\x03\x02\x02\x02\u0163\u0161\x03\x02\x02\x02\u0163\u0162\x03\x02" +
		"\x02\x02\u0164W\x03\x02\x02\x02\u0165\u0170\x05T+\x02\u0166\u0167\x07" +
		"\x1D\x02\x02\u0167\u016E\x05R*\x02\u0168\u0169\x07\x1E\x02\x02\u0169\u016C" +
		"\x05R*\x02\u016A\u016B\x07\x1F\x02\x02\u016B\u016D\x05R*\x02\u016C\u016A" +
		"\x03\x02\x02\x02\u016C\u016D\x03\x02\x02\x02\u016D\u016F\x03\x02\x02\x02" +
		"\u016E\u0168\x03\x02\x02\x02\u016E\u016F\x03\x02\x02\x02\u016F\u0171\x03" +
		"\x02\x02\x02\u0170\u0166\x03\x02\x02\x02\u0170\u0171\x03\x02\x02\x02\u0171" +
		"Y\x03\x02\x02\x02/_ckoty\x83\x8D\x92\x97\xA3\xA7\xB4\xB7\xBB\xCF\xD4\xD7" +
		"\xE0\xE5\xE8\xEC\xEF\xF3\xF6\xFA\xFD\u0104\u0111\u0117\u011E\u0122\u0126" +
		"\u012A\u012F\u0135\u0139\u014A\u014E\u0153\u0158\u0163\u016C\u016E\u0170";
	public static __ATN: ATN;
	public static get _ATN(): ATN {
		if (!AxeTemplateParser.__ATN) {
			AxeTemplateParser.__ATN = new ATNDeserializer().deserialize(Utils.toCharArray(AxeTemplateParser._serializedATN));
		}

		return AxeTemplateParser.__ATN;
	}

}

export class TemplateContext extends ParserRuleContext {
	public header(): HeaderContext {
		return this.getRuleContext(0, HeaderContext);
	}
	public metaDef(): MetaDefContext {
		return this.getRuleContext(0, MetaDefContext);
	}
	public srcItem(): SrcItemContext[];
	public srcItem(i: number): SrcItemContext;
	public srcItem(i?: number): SrcItemContext | SrcItemContext[] {
		if (i === undefined) {
			return this.getRuleContexts(SrcItemContext);
		} else {
			return this.getRuleContext(i, SrcItemContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_template; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterTemplate) {
			listener.enterTemplate(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitTemplate) {
			listener.exitTemplate(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitTemplate) {
			return visitor.visitTemplate(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class HeaderContext extends ParserRuleContext {
	public _ver!: QuotedStrContext;
	public _desc!: QuotedStrContext;
	public quotedStr(): QuotedStrContext[];
	public quotedStr(i: number): QuotedStrContext;
	public quotedStr(i?: number): QuotedStrContext | QuotedStrContext[] {
		if (i === undefined) {
			return this.getRuleContexts(QuotedStrContext);
		} else {
			return this.getRuleContext(i, QuotedStrContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_header; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterHeader) {
			listener.enterHeader(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitHeader) {
			listener.exitHeader(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitHeader) {
			return visitor.visitHeader(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class MetaDefContext extends ParserRuleContext {
	public _r!: MetaRoleDefContext;
	public _v!: MetaVarDefContext;
	public metaRoleDef(): MetaRoleDefContext[];
	public metaRoleDef(i: number): MetaRoleDefContext;
	public metaRoleDef(i?: number): MetaRoleDefContext | MetaRoleDefContext[] {
		if (i === undefined) {
			return this.getRuleContexts(MetaRoleDefContext);
		} else {
			return this.getRuleContext(i, MetaRoleDefContext);
		}
	}
	public metaVarDef(): MetaVarDefContext[];
	public metaVarDef(i: number): MetaVarDefContext;
	public metaVarDef(i?: number): MetaVarDefContext | MetaVarDefContext[] {
		if (i === undefined) {
			return this.getRuleContexts(MetaVarDefContext);
		} else {
			return this.getRuleContext(i, MetaVarDefContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_metaDef; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterMetaDef) {
			listener.enterMetaDef(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitMetaDef) {
			listener.exitMetaDef(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitMetaDef) {
			return visitor.visitMetaDef(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class MetaRoleDefContext extends ParserRuleContext {
	public _role!: RoleIdContext;
	public _desc!: QuotedStrContext;
	public roleId(): RoleIdContext {
		return this.getRuleContext(0, RoleIdContext);
	}
	public quotedStr(): QuotedStrContext | undefined {
		return this.tryGetRuleContext(0, QuotedStrContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_metaRoleDef; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterMetaRoleDef) {
			listener.enterMetaRoleDef(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitMetaRoleDef) {
			listener.exitMetaRoleDef(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitMetaRoleDef) {
			return visitor.visitMetaRoleDef(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class MetaVarDefContext extends ParserRuleContext {
	public _val!: MetaVarNameContext;
	public _type!: VarTypeContext;
	public _desc!: QuotedStrContext;
	public metaVarName(): MetaVarNameContext {
		return this.getRuleContext(0, MetaVarNameContext);
	}
	public varType(): VarTypeContext {
		return this.getRuleContext(0, VarTypeContext);
	}
	public quotedStr(): QuotedStrContext | undefined {
		return this.tryGetRuleContext(0, QuotedStrContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_metaVarDef; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterMetaVarDef) {
			listener.enterMetaVarDef(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitMetaVarDef) {
			listener.exitMetaVarDef(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitMetaVarDef) {
			return visitor.visitMetaVarDef(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class MetaVarNameContext extends ParserRuleContext {
	public ID(): TerminalNode { return this.getToken(AxeTemplateParser.ID, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_metaVarName; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterMetaVarName) {
			listener.enterMetaVarName(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitMetaVarName) {
			listener.exitMetaVarName(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitMetaVarName) {
			return visitor.visitMetaVarName(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class VarTypeContext extends ParserRuleContext {
	public _t!: Token;
	public _n!: Token;
	public _p!: Token;
	public _dt!: Token;
	public _ct!: Token;
	public _un!: Token;
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_varType; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterVarType) {
			listener.enterVarType(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitVarType) {
			listener.exitVarType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitVarType) {
			return visitor.visitVarType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SrcItemContext extends ParserRuleContext {
	public _lbl!: LabelItemContext;
	public _role!: RoleVarItemContext;
	public _var!: VarDeclItemContext;
	public _inc!: IncludeItemContext;
	public _acc!: AccountStmtContext;
	public _cass!: CommodityStmtContext;
	public _dass!: DiscreteStmtContext;
	public _cmt!: CommentItemContext;
	public labelItem(): LabelItemContext | undefined {
		return this.tryGetRuleContext(0, LabelItemContext);
	}
	public roleVarItem(): RoleVarItemContext | undefined {
		return this.tryGetRuleContext(0, RoleVarItemContext);
	}
	public varDeclItem(): VarDeclItemContext | undefined {
		return this.tryGetRuleContext(0, VarDeclItemContext);
	}
	public includeItem(): IncludeItemContext | undefined {
		return this.tryGetRuleContext(0, IncludeItemContext);
	}
	public accountStmt(): AccountStmtContext | undefined {
		return this.tryGetRuleContext(0, AccountStmtContext);
	}
	public commodityStmt(): CommodityStmtContext | undefined {
		return this.tryGetRuleContext(0, CommodityStmtContext);
	}
	public discreteStmt(): DiscreteStmtContext | undefined {
		return this.tryGetRuleContext(0, DiscreteStmtContext);
	}
	public commentItem(): CommentItemContext | undefined {
		return this.tryGetRuleContext(0, CommentItemContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_srcItem; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterSrcItem) {
			listener.enterSrcItem(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitSrcItem) {
			listener.exitSrcItem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitSrcItem) {
			return visitor.visitSrcItem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class CommentItemContext extends ParserRuleContext {
	public COMMENT(): TerminalNode[];
	public COMMENT(i: number): TerminalNode;
	public COMMENT(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(AxeTemplateParser.COMMENT);
		} else {
			return this.getToken(AxeTemplateParser.COMMENT, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_commentItem; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterCommentItem) {
			listener.enterCommentItem(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitCommentItem) {
			listener.exitCommentItem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitCommentItem) {
			return visitor.visitCommentItem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class LabelItemContext extends ParserRuleContext {
	public _index!: Token;
	public _desc!: QuotedStrContext;
	public quotedStr(): QuotedStrContext {
		return this.getRuleContext(0, QuotedStrContext);
	}
	public INT(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.INT, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_labelItem; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterLabelItem) {
			listener.enterLabelItem(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitLabelItem) {
			listener.exitLabelItem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitLabelItem) {
			return visitor.visitLabelItem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class VarDeclItemContext extends ParserRuleContext {
	public _name!: VarNameContext;
	public _init!: VarInitExprContext;
	public varName(): VarNameContext {
		return this.getRuleContext(0, VarNameContext);
	}
	public varInitExpr(): VarInitExprContext {
		return this.getRuleContext(0, VarInitExprContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_varDeclItem; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterVarDeclItem) {
			listener.enterVarDeclItem(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitVarDeclItem) {
			listener.exitVarDeclItem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitVarDeclItem) {
			return visitor.visitVarDeclItem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class VarNameContext extends ParserRuleContext {
	public _id!: Token;
	public _xid!: Token;
	public ID(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.ID, 0); }
	public EXT_ID(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.EXT_ID, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_varName; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterVarName) {
			listener.enterVarName(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitVarName) {
			listener.exitVarName(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitVarName) {
			return visitor.visitVarName(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class VarInitExprContext extends ParserRuleContext {
	public _nval!: NumVarExprContext;
	public _sval!: StrVarExprContext;
	public numVarExpr(): NumVarExprContext | undefined {
		return this.tryGetRuleContext(0, NumVarExprContext);
	}
	public strVarExpr(): StrVarExprContext | undefined {
		return this.tryGetRuleContext(0, StrVarExprContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_varInitExpr; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterVarInitExpr) {
			listener.enterVarInitExpr(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitVarInitExpr) {
			listener.exitVarInitExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitVarInitExpr) {
			return visitor.visitVarInitExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RoleVarItemContext extends ParserRuleContext {
	public _alias!: RoleAliasContext;
	public _role!: RoleIdContext;
	public roleAlias(): RoleAliasContext {
		return this.getRuleContext(0, RoleAliasContext);
	}
	public roleId(): RoleIdContext {
		return this.getRuleContext(0, RoleIdContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_roleVarItem; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterRoleVarItem) {
			listener.enterRoleVarItem(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitRoleVarItem) {
			listener.exitRoleVarItem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitRoleVarItem) {
			return visitor.visitRoleVarItem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class IncludeItemContext extends ParserRuleContext {
	public _tid!: QuotedStrContext;
	public quotedStr(): QuotedStrContext {
		return this.getRuleContext(0, QuotedStrContext);
	}
	public localScope(): LocalScopeContext[];
	public localScope(i: number): LocalScopeContext;
	public localScope(i?: number): LocalScopeContext | LocalScopeContext[] {
		if (i === undefined) {
			return this.getRuleContexts(LocalScopeContext);
		} else {
			return this.getRuleContext(i, LocalScopeContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_includeItem; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterIncludeItem) {
			listener.enterIncludeItem(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitIncludeItem) {
			listener.exitIncludeItem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitIncludeItem) {
			return visitor.visitIncludeItem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class LocalScopeContext extends ParserRuleContext {
	public _role!: RoleLocalScopeContext;
	public _val!: VarLocalScopeContext;
	public roleLocalScope(): RoleLocalScopeContext | undefined {
		return this.tryGetRuleContext(0, RoleLocalScopeContext);
	}
	public varLocalScope(): VarLocalScopeContext | undefined {
		return this.tryGetRuleContext(0, VarLocalScopeContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_localScope; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterLocalScope) {
			listener.enterLocalScope(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitLocalScope) {
			listener.exitLocalScope(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitLocalScope) {
			return visitor.visitLocalScope(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RoleLocalScopeContext extends ParserRuleContext {
	public _inner!: RoleIdContext;
	public _outer!: RoleIdContext;
	public roleId(): RoleIdContext[];
	public roleId(i: number): RoleIdContext;
	public roleId(i?: number): RoleIdContext | RoleIdContext[] {
		if (i === undefined) {
			return this.getRuleContexts(RoleIdContext);
		} else {
			return this.getRuleContext(i, RoleIdContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_roleLocalScope; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterRoleLocalScope) {
			listener.enterRoleLocalScope(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitRoleLocalScope) {
			listener.exitRoleLocalScope(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitRoleLocalScope) {
			return visitor.visitRoleLocalScope(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class VarLocalScopeContext extends ParserRuleContext {
	public _name!: VarNameContext;
	public _init!: VarInitExprContext;
	public varName(): VarNameContext {
		return this.getRuleContext(0, VarNameContext);
	}
	public varInitExpr(): VarInitExprContext {
		return this.getRuleContext(0, VarInitExprContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_varLocalScope; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterVarLocalScope) {
			listener.enterVarLocalScope(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitVarLocalScope) {
			listener.exitVarLocalScope(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitVarLocalScope) {
			return visitor.visitVarLocalScope(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AccountStmtContext extends ParserRuleContext {
	public _role!: TemplateRoleContext;
	public _left!: AccountSetterListContext;
	public _right!: AccountSetterListContext;
	public templateRole(): TemplateRoleContext {
		return this.getRuleContext(0, TemplateRoleContext);
	}
	public accountSetterList(): AccountSetterListContext[];
	public accountSetterList(i: number): AccountSetterListContext;
	public accountSetterList(i?: number): AccountSetterListContext | AccountSetterListContext[] {
		if (i === undefined) {
			return this.getRuleContexts(AccountSetterListContext);
		} else {
			return this.getRuleContext(i, AccountSetterListContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_accountStmt; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterAccountStmt) {
			listener.enterAccountStmt(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitAccountStmt) {
			listener.exitAccountStmt(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitAccountStmt) {
			return visitor.visitAccountStmt(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AccountSetterListContext extends ParserRuleContext {
	public accountSetter(): AccountSetterContext[];
	public accountSetter(i: number): AccountSetterContext;
	public accountSetter(i?: number): AccountSetterContext | AccountSetterContext[] {
		if (i === undefined) {
			return this.getRuleContexts(AccountSetterContext);
		} else {
			return this.getRuleContext(i, AccountSetterContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_accountSetterList; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterAccountSetterList) {
			listener.enterAccountSetterList(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitAccountSetterList) {
			listener.exitAccountSetterList(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitAccountSetterList) {
			return visitor.visitAccountSetterList(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AccountSetterContext extends ParserRuleContext {
	public _accId!: AccountIdContext;
	public _title!: QuotedStrContext;
	public _val!: ValueContext;
	public accountId(): AccountIdContext {
		return this.getRuleContext(0, AccountIdContext);
	}
	public quotedStr(): QuotedStrContext | undefined {
		return this.tryGetRuleContext(0, QuotedStrContext);
	}
	public value(): ValueContext | undefined {
		return this.tryGetRuleContext(0, ValueContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_accountSetter; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterAccountSetter) {
			listener.enterAccountSetter(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitAccountSetter) {
			listener.exitAccountSetter(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitAccountSetter) {
			return visitor.visitAccountSetter(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AccountIdContext extends ParserRuleContext {
	public _number!: AccountNumberContext;
	public accountNumber(): AccountNumberContext {
		return this.getRuleContext(0, AccountNumberContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_accountId; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterAccountId) {
			listener.enterAccountId(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitAccountId) {
			listener.exitAccountId(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitAccountId) {
			return visitor.visitAccountId(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class CommodityStmtContext extends ParserRuleContext {
	public _asset!: CommodityStmtAssetContext;
	public _adj!: AssetAdjustmentContext;
	public commodityStmtAsset(): CommodityStmtAssetContext {
		return this.getRuleContext(0, CommodityStmtAssetContext);
	}
	public assetAdjustment(): AssetAdjustmentContext {
		return this.getRuleContext(0, AssetAdjustmentContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_commodityStmt; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterCommodityStmt) {
			listener.enterCommodityStmt(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitCommodityStmt) {
			listener.exitCommodityStmt(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitCommodityStmt) {
			return visitor.visitCommodityStmt(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class CommodityStmtAssetContext extends ParserRuleContext {
	public _swc!: Token;
	public _swcQty!: QtyContext;
	public _brc!: Token;
	public _brcUnit!: UnitContext;
	public _brcQty!: QtyContext;
	public _fiat!: Token;
	public _fiatQty!: QtyContext;
	public _fiatUnit!: UnitContext;
	public _crypto!: Token;
	public _cryptoQty!: QtyContext;
	public _cryptoUnit!: UnitContext;
	public _virtual!: Token;
	public _virtualQty!: QtyContext;
	public _virtualUnit!: UnitContext;
	public _typeId!: AssetTypeIdContext;
	public _macQty!: QtyContext;
	public _macUnit!: UnitContext;
	public SWC(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.SWC, 0); }
	public qty(): QtyContext | undefined {
		return this.tryGetRuleContext(0, QtyContext);
	}
	public BRC(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.BRC, 0); }
	public unit(): UnitContext | undefined {
		return this.tryGetRuleContext(0, UnitContext);
	}
	public FIAT(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.FIAT, 0); }
	public CRYPTO(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.CRYPTO, 0); }
	public VIRTUAL(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.VIRTUAL, 0); }
	public COMMODITY(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.COMMODITY, 0); }
	public assetTypeId(): AssetTypeIdContext | undefined {
		return this.tryGetRuleContext(0, AssetTypeIdContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_commodityStmtAsset; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterCommodityStmtAsset) {
			listener.enterCommodityStmtAsset(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitCommodityStmtAsset) {
			listener.exitCommodityStmtAsset(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitCommodityStmtAsset) {
			return visitor.visitCommodityStmtAsset(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class DiscreteStmtContext extends ParserRuleContext {
	public _asset!: DiscreteStmtAssetContext;
	public _adj!: AssetAdjustmentContext;
	public DISCRETE(): TerminalNode { return this.getToken(AxeTemplateParser.DISCRETE, 0); }
	public discreteStmtAsset(): DiscreteStmtAssetContext {
		return this.getRuleContext(0, DiscreteStmtAssetContext);
	}
	public assetAdjustment(): AssetAdjustmentContext {
		return this.getRuleContext(0, AssetAdjustmentContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_discreteStmt; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterDiscreteStmt) {
			listener.enterDiscreteStmt(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitDiscreteStmt) {
			listener.exitDiscreteStmt(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitDiscreteStmt) {
			return visitor.visitDiscreteStmt(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class DiscreteStmtAssetContext extends ParserRuleContext {
	public _typeId!: AssetTypeIdContext;
	public _macAssetId!: AssetIdContext;
	public _macShare!: ShareContext;
	public assetTypeId(): AssetTypeIdContext {
		return this.getRuleContext(0, AssetTypeIdContext);
	}
	public assetId(): AssetIdContext {
		return this.getRuleContext(0, AssetIdContext);
	}
	public share(): ShareContext {
		return this.getRuleContext(0, ShareContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_discreteStmtAsset; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterDiscreteStmtAsset) {
			listener.enterDiscreteStmtAsset(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitDiscreteStmtAsset) {
			listener.exitDiscreteStmtAsset(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitDiscreteStmtAsset) {
			return visitor.visitDiscreteStmtAsset(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AssetAdjustmentContext extends ParserRuleContext {
	public _role!: TemplateRoleContext;
	public _incr!: Token;
	public _decr!: Token;
	public _from!: TemplateRoleContext;
	public _to!: TemplateRoleContext;
	public templateRole(): TemplateRoleContext[];
	public templateRole(i: number): TemplateRoleContext;
	public templateRole(i?: number): TemplateRoleContext | TemplateRoleContext[] {
		if (i === undefined) {
			return this.getRuleContexts(TemplateRoleContext);
		} else {
			return this.getRuleContext(i, TemplateRoleContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_assetAdjustment; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterAssetAdjustment) {
			listener.enterAssetAdjustment(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitAssetAdjustment) {
			listener.exitAssetAdjustment(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitAssetAdjustment) {
			return visitor.visitAssetAdjustment(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AssetIdContext extends ParserRuleContext {
	public _rf!: Token;
	public VAR_REF(): TerminalNode { return this.getToken(AxeTemplateParser.VAR_REF, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_assetId; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterAssetId) {
			listener.enterAssetId(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitAssetId) {
			listener.exitAssetId(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitAssetId) {
			return visitor.visitAssetId(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ValueContext extends ParserRuleContext {
	public _remainder!: Token;
	public _num!: ExtNumContext;
	public _form!: ExprContext;
	public extNum(): ExtNumContext | undefined {
		return this.tryGetRuleContext(0, ExtNumContext);
	}
	public expr(): ExprContext | undefined {
		return this.tryGetRuleContext(0, ExprContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_value; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterValue) {
			listener.enterValue(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitValue) {
			listener.exitValue(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitValue) {
			return visitor.visitValue(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class UnitContext extends ParserRuleContext {
	public _id!: ExtIdContext;
	public _rf!: Token;
	public extId(): ExtIdContext | undefined {
		return this.tryGetRuleContext(0, ExtIdContext);
	}
	public VAR_REF(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.VAR_REF, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_unit; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterUnit) {
			listener.enterUnit(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitUnit) {
			listener.exitUnit(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitUnit) {
			return visitor.visitUnit(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class QtyContext extends ParserRuleContext {
	public _lit!: ExtNumContext;
	public _form!: ExprContext;
	public extNum(): ExtNumContext | undefined {
		return this.tryGetRuleContext(0, ExtNumContext);
	}
	public expr(): ExprContext | undefined {
		return this.tryGetRuleContext(0, ExprContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_qty; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterQty) {
			listener.enterQty(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitQty) {
			listener.exitQty(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitQty) {
			return visitor.visitQty(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ShareContext extends ParserRuleContext {
	public _lit!: Token;
	public _form!: ExprContext;
	public PERCENT(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.PERCENT, 0); }
	public expr(): ExprContext | undefined {
		return this.tryGetRuleContext(0, ExprContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_share; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterShare) {
			listener.enterShare(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitShare) {
			listener.exitShare(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitShare) {
			return visitor.visitShare(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class NumVarExprContext extends ParserRuleContext {
	public _form!: ExprContext;
	public _perc!: Token;
	public _noPerc!: ExtNumContext;
	public expr(): ExprContext | undefined {
		return this.tryGetRuleContext(0, ExprContext);
	}
	public PERCENT(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.PERCENT, 0); }
	public extNum(): ExtNumContext | undefined {
		return this.tryGetRuleContext(0, ExtNumContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_numVarExpr; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterNumVarExpr) {
			listener.enterNumVarExpr(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitNumVarExpr) {
			listener.exitNumVarExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitNumVarExpr) {
			return visitor.visitNumVarExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class StrVarExprContext extends ParserRuleContext {
	public _lit!: QuotedStrContext;
	public quotedStr(): QuotedStrContext {
		return this.getRuleContext(0, QuotedStrContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_strVarExpr; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterStrVarExpr) {
			listener.enterStrVarExpr(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitStrVarExpr) {
			listener.exitStrVarExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitStrVarExpr) {
			return visitor.visitStrVarExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ExprContext extends ParserRuleContext {
	public _form!: Token;
	public _ref!: Token;
	public FORMULA(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.FORMULA, 0); }
	public VAR_REF(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.VAR_REF, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_expr; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterExpr) {
			listener.enterExpr(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitExpr) {
			listener.exitExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitExpr) {
			return visitor.visitExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TemplateRoleContext extends ParserRuleContext {
	public roleId(): RoleIdContext | undefined {
		return this.tryGetRuleContext(0, RoleIdContext);
	}
	public roleAlias(): RoleAliasContext | undefined {
		return this.tryGetRuleContext(0, RoleAliasContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_templateRole; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterTemplateRole) {
			listener.enterTemplateRole(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitTemplateRole) {
			listener.exitTemplateRole(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitTemplateRole) {
			return visitor.visitTemplateRole(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RoleAliasContext extends ParserRuleContext {
	public _id!: ExtIdContext;
	public ROLE_ALIAS_PREFIX(): TerminalNode { return this.getToken(AxeTemplateParser.ROLE_ALIAS_PREFIX, 0); }
	public extId(): ExtIdContext {
		return this.getRuleContext(0, ExtIdContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_roleAlias; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterRoleAlias) {
			listener.enterRoleAlias(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitRoleAlias) {
			listener.exitRoleAlias(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitRoleAlias) {
			return visitor.visitRoleAlias(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RoleIdContext extends ParserRuleContext {
	public _id!: ExtIdContext;
	public ROLE_PREFIX(): TerminalNode { return this.getToken(AxeTemplateParser.ROLE_PREFIX, 0); }
	public extId(): ExtIdContext {
		return this.getRuleContext(0, ExtIdContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_roleId; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterRoleId) {
			listener.enterRoleId(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitRoleId) {
			listener.exitRoleId(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitRoleId) {
			return visitor.visitRoleId(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AccountNumberContext extends ParserRuleContext {
	public _num!: Token;
	public _id!: Token;
	public INT(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.INT, 0); }
	public ID(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.ID, 0); }
	public COMMODITY(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.COMMODITY, 0); }
	public DISCRETE(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.DISCRETE, 0); }
	public SWC(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.SWC, 0); }
	public BRC(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.BRC, 0); }
	public FIAT(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.FIAT, 0); }
	public CRYPTO(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.CRYPTO, 0); }
	public VIRTUAL(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.VIRTUAL, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_accountNumber; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterAccountNumber) {
			listener.enterAccountNumber(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitAccountNumber) {
			listener.exitAccountNumber(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitAccountNumber) {
			return visitor.visitAccountNumber(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ExtNumContext extends ParserRuleContext {
	public _dec!: Token;
	public _int!: Token;
	public DEC(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.DEC, 0); }
	public INT(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.INT, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_extNum; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterExtNum) {
			listener.enterExtNum(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitExtNum) {
			listener.exitExtNum(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitExtNum) {
			return visitor.visitExtNum(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ExtIdContext extends ParserRuleContext {
	public _qs!: QuotedStrContext;
	public _id!: Token;
	public _ext!: Token;
	public quotedStr(): QuotedStrContext | undefined {
		return this.tryGetRuleContext(0, QuotedStrContext);
	}
	public ID(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.ID, 0); }
	public EXT_ID(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.EXT_ID, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_extId; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterExtId) {
			listener.enterExtId(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitExtId) {
			listener.exitExtId(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitExtId) {
			return visitor.visitExtId(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class QuotedStrContext extends ParserRuleContext {
	public _dq!: Token;
	public _sq!: Token;
	public _ts!: Token;
	public DQ_STRING(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.DQ_STRING, 0); }
	public SQ_STRING(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.SQ_STRING, 0); }
	public TICK_STRING(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.TICK_STRING, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_quotedStr; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterQuotedStr) {
			listener.enterQuotedStr(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitQuotedStr) {
			listener.exitQuotedStr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitQuotedStr) {
			return visitor.visitQuotedStr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AssetTypeIdContext extends ParserRuleContext {
	public ID(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.ID, 0); }
	public EXT_ID(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.EXT_ID, 0); }
	public COMMODITY(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.COMMODITY, 0); }
	public DISCRETE(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.DISCRETE, 0); }
	public SWC(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.SWC, 0); }
	public BRC(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.BRC, 0); }
	public FIAT(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.FIAT, 0); }
	public CRYPTO(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.CRYPTO, 0); }
	public VIRTUAL(): TerminalNode | undefined { return this.tryGetToken(AxeTemplateParser.VIRTUAL, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_assetTypeId; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterAssetTypeId) {
			listener.enterAssetTypeId(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitAssetTypeId) {
			listener.exitAssetTypeId(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitAssetTypeId) {
			return visitor.visitAssetTypeId(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SignatureContext extends ParserRuleContext {
	public _val!: QuotedStrContext;
	public _signer!: ExtIdContext;
	public _mandator!: ExtIdContext;
	public _mandate!: ExtIdContext;
	public quotedStr(): QuotedStrContext {
		return this.getRuleContext(0, QuotedStrContext);
	}
	public extId(): ExtIdContext[];
	public extId(i: number): ExtIdContext;
	public extId(i?: number): ExtIdContext | ExtIdContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExtIdContext);
		} else {
			return this.getRuleContext(i, ExtIdContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return AxeTemplateParser.RULE_signature; }
	// @Override
	public enterRule(listener: AxeTemplateListener): void {
		if (listener.enterSignature) {
			listener.enterSignature(this);
		}
	}
	// @Override
	public exitRule(listener: AxeTemplateListener): void {
		if (listener.exitSignature) {
			listener.exitSignature(this);
		}
	}
	// @Override
	public accept<Result>(visitor: AxeTemplateVisitor<Result>): Result {
		if (visitor.visitSignature) {
			return visitor.visitSignature(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


