// Generated from syntaxes/grammars/AxeTemplate.g4 by ANTLR 4.9.0-SNAPSHOT


import { ParseTreeListener } from "antlr4ts/tree/ParseTreeListener";

import { TemplateContext } from "./AxeTemplateParser";
import { HeaderContext } from "./AxeTemplateParser";
import { MetaDefContext } from "./AxeTemplateParser";
import { MetaRoleDefContext } from "./AxeTemplateParser";
import { MetaVarDefContext } from "./AxeTemplateParser";
import { MetaVarNameContext } from "./AxeTemplateParser";
import { VarTypeContext } from "./AxeTemplateParser";
import { SrcItemContext } from "./AxeTemplateParser";
import { CommentItemContext } from "./AxeTemplateParser";
import { LabelItemContext } from "./AxeTemplateParser";
import { VarDeclItemContext } from "./AxeTemplateParser";
import { VarNameContext } from "./AxeTemplateParser";
import { VarInitExprContext } from "./AxeTemplateParser";
import { RoleVarItemContext } from "./AxeTemplateParser";
import { IncludeItemContext } from "./AxeTemplateParser";
import { LocalScopeContext } from "./AxeTemplateParser";
import { RoleLocalScopeContext } from "./AxeTemplateParser";
import { VarLocalScopeContext } from "./AxeTemplateParser";
import { AccountStmtContext } from "./AxeTemplateParser";
import { AccountSetterListContext } from "./AxeTemplateParser";
import { AccountSetterContext } from "./AxeTemplateParser";
import { AccountIdContext } from "./AxeTemplateParser";
import { CommodityStmtContext } from "./AxeTemplateParser";
import { CommodityStmtAssetContext } from "./AxeTemplateParser";
import { DiscreteStmtContext } from "./AxeTemplateParser";
import { DiscreteStmtAssetContext } from "./AxeTemplateParser";
import { AssetAdjustmentContext } from "./AxeTemplateParser";
import { AssetIdContext } from "./AxeTemplateParser";
import { ValueContext } from "./AxeTemplateParser";
import { UnitContext } from "./AxeTemplateParser";
import { QtyContext } from "./AxeTemplateParser";
import { ShareContext } from "./AxeTemplateParser";
import { NumVarExprContext } from "./AxeTemplateParser";
import { StrVarExprContext } from "./AxeTemplateParser";
import { ExprContext } from "./AxeTemplateParser";
import { TemplateRoleContext } from "./AxeTemplateParser";
import { RoleAliasContext } from "./AxeTemplateParser";
import { RoleIdContext } from "./AxeTemplateParser";
import { AccountNumberContext } from "./AxeTemplateParser";
import { ExtNumContext } from "./AxeTemplateParser";
import { ExtIdContext } from "./AxeTemplateParser";
import { QuotedStrContext } from "./AxeTemplateParser";
import { AssetTypeIdContext } from "./AxeTemplateParser";
import { SignatureContext } from "./AxeTemplateParser";


/**
 * This interface defines a complete listener for a parse tree produced by
 * `AxeTemplateParser`.
 */
export interface AxeTemplateListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by `AxeTemplateParser.template`.
	 * @param ctx the parse tree
	 */
	enterTemplate?: (ctx: TemplateContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.template`.
	 * @param ctx the parse tree
	 */
	exitTemplate?: (ctx: TemplateContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.header`.
	 * @param ctx the parse tree
	 */
	enterHeader?: (ctx: HeaderContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.header`.
	 * @param ctx the parse tree
	 */
	exitHeader?: (ctx: HeaderContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.metaDef`.
	 * @param ctx the parse tree
	 */
	enterMetaDef?: (ctx: MetaDefContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.metaDef`.
	 * @param ctx the parse tree
	 */
	exitMetaDef?: (ctx: MetaDefContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.metaRoleDef`.
	 * @param ctx the parse tree
	 */
	enterMetaRoleDef?: (ctx: MetaRoleDefContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.metaRoleDef`.
	 * @param ctx the parse tree
	 */
	exitMetaRoleDef?: (ctx: MetaRoleDefContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.metaVarDef`.
	 * @param ctx the parse tree
	 */
	enterMetaVarDef?: (ctx: MetaVarDefContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.metaVarDef`.
	 * @param ctx the parse tree
	 */
	exitMetaVarDef?: (ctx: MetaVarDefContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.metaVarName`.
	 * @param ctx the parse tree
	 */
	enterMetaVarName?: (ctx: MetaVarNameContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.metaVarName`.
	 * @param ctx the parse tree
	 */
	exitMetaVarName?: (ctx: MetaVarNameContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.varType`.
	 * @param ctx the parse tree
	 */
	enterVarType?: (ctx: VarTypeContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.varType`.
	 * @param ctx the parse tree
	 */
	exitVarType?: (ctx: VarTypeContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.srcItem`.
	 * @param ctx the parse tree
	 */
	enterSrcItem?: (ctx: SrcItemContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.srcItem`.
	 * @param ctx the parse tree
	 */
	exitSrcItem?: (ctx: SrcItemContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.commentItem`.
	 * @param ctx the parse tree
	 */
	enterCommentItem?: (ctx: CommentItemContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.commentItem`.
	 * @param ctx the parse tree
	 */
	exitCommentItem?: (ctx: CommentItemContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.labelItem`.
	 * @param ctx the parse tree
	 */
	enterLabelItem?: (ctx: LabelItemContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.labelItem`.
	 * @param ctx the parse tree
	 */
	exitLabelItem?: (ctx: LabelItemContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.varDeclItem`.
	 * @param ctx the parse tree
	 */
	enterVarDeclItem?: (ctx: VarDeclItemContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.varDeclItem`.
	 * @param ctx the parse tree
	 */
	exitVarDeclItem?: (ctx: VarDeclItemContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.varName`.
	 * @param ctx the parse tree
	 */
	enterVarName?: (ctx: VarNameContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.varName`.
	 * @param ctx the parse tree
	 */
	exitVarName?: (ctx: VarNameContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.varInitExpr`.
	 * @param ctx the parse tree
	 */
	enterVarInitExpr?: (ctx: VarInitExprContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.varInitExpr`.
	 * @param ctx the parse tree
	 */
	exitVarInitExpr?: (ctx: VarInitExprContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.roleVarItem`.
	 * @param ctx the parse tree
	 */
	enterRoleVarItem?: (ctx: RoleVarItemContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.roleVarItem`.
	 * @param ctx the parse tree
	 */
	exitRoleVarItem?: (ctx: RoleVarItemContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.includeItem`.
	 * @param ctx the parse tree
	 */
	enterIncludeItem?: (ctx: IncludeItemContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.includeItem`.
	 * @param ctx the parse tree
	 */
	exitIncludeItem?: (ctx: IncludeItemContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.localScope`.
	 * @param ctx the parse tree
	 */
	enterLocalScope?: (ctx: LocalScopeContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.localScope`.
	 * @param ctx the parse tree
	 */
	exitLocalScope?: (ctx: LocalScopeContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.roleLocalScope`.
	 * @param ctx the parse tree
	 */
	enterRoleLocalScope?: (ctx: RoleLocalScopeContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.roleLocalScope`.
	 * @param ctx the parse tree
	 */
	exitRoleLocalScope?: (ctx: RoleLocalScopeContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.varLocalScope`.
	 * @param ctx the parse tree
	 */
	enterVarLocalScope?: (ctx: VarLocalScopeContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.varLocalScope`.
	 * @param ctx the parse tree
	 */
	exitVarLocalScope?: (ctx: VarLocalScopeContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.accountStmt`.
	 * @param ctx the parse tree
	 */
	enterAccountStmt?: (ctx: AccountStmtContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.accountStmt`.
	 * @param ctx the parse tree
	 */
	exitAccountStmt?: (ctx: AccountStmtContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.accountSetterList`.
	 * @param ctx the parse tree
	 */
	enterAccountSetterList?: (ctx: AccountSetterListContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.accountSetterList`.
	 * @param ctx the parse tree
	 */
	exitAccountSetterList?: (ctx: AccountSetterListContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.accountSetter`.
	 * @param ctx the parse tree
	 */
	enterAccountSetter?: (ctx: AccountSetterContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.accountSetter`.
	 * @param ctx the parse tree
	 */
	exitAccountSetter?: (ctx: AccountSetterContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.accountId`.
	 * @param ctx the parse tree
	 */
	enterAccountId?: (ctx: AccountIdContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.accountId`.
	 * @param ctx the parse tree
	 */
	exitAccountId?: (ctx: AccountIdContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.commodityStmt`.
	 * @param ctx the parse tree
	 */
	enterCommodityStmt?: (ctx: CommodityStmtContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.commodityStmt`.
	 * @param ctx the parse tree
	 */
	exitCommodityStmt?: (ctx: CommodityStmtContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.commodityStmtAsset`.
	 * @param ctx the parse tree
	 */
	enterCommodityStmtAsset?: (ctx: CommodityStmtAssetContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.commodityStmtAsset`.
	 * @param ctx the parse tree
	 */
	exitCommodityStmtAsset?: (ctx: CommodityStmtAssetContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.discreteStmt`.
	 * @param ctx the parse tree
	 */
	enterDiscreteStmt?: (ctx: DiscreteStmtContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.discreteStmt`.
	 * @param ctx the parse tree
	 */
	exitDiscreteStmt?: (ctx: DiscreteStmtContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.discreteStmtAsset`.
	 * @param ctx the parse tree
	 */
	enterDiscreteStmtAsset?: (ctx: DiscreteStmtAssetContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.discreteStmtAsset`.
	 * @param ctx the parse tree
	 */
	exitDiscreteStmtAsset?: (ctx: DiscreteStmtAssetContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.assetAdjustment`.
	 * @param ctx the parse tree
	 */
	enterAssetAdjustment?: (ctx: AssetAdjustmentContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.assetAdjustment`.
	 * @param ctx the parse tree
	 */
	exitAssetAdjustment?: (ctx: AssetAdjustmentContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.assetId`.
	 * @param ctx the parse tree
	 */
	enterAssetId?: (ctx: AssetIdContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.assetId`.
	 * @param ctx the parse tree
	 */
	exitAssetId?: (ctx: AssetIdContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.value`.
	 * @param ctx the parse tree
	 */
	enterValue?: (ctx: ValueContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.value`.
	 * @param ctx the parse tree
	 */
	exitValue?: (ctx: ValueContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.unit`.
	 * @param ctx the parse tree
	 */
	enterUnit?: (ctx: UnitContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.unit`.
	 * @param ctx the parse tree
	 */
	exitUnit?: (ctx: UnitContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.qty`.
	 * @param ctx the parse tree
	 */
	enterQty?: (ctx: QtyContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.qty`.
	 * @param ctx the parse tree
	 */
	exitQty?: (ctx: QtyContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.share`.
	 * @param ctx the parse tree
	 */
	enterShare?: (ctx: ShareContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.share`.
	 * @param ctx the parse tree
	 */
	exitShare?: (ctx: ShareContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.numVarExpr`.
	 * @param ctx the parse tree
	 */
	enterNumVarExpr?: (ctx: NumVarExprContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.numVarExpr`.
	 * @param ctx the parse tree
	 */
	exitNumVarExpr?: (ctx: NumVarExprContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.strVarExpr`.
	 * @param ctx the parse tree
	 */
	enterStrVarExpr?: (ctx: StrVarExprContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.strVarExpr`.
	 * @param ctx the parse tree
	 */
	exitStrVarExpr?: (ctx: StrVarExprContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.expr`.
	 * @param ctx the parse tree
	 */
	enterExpr?: (ctx: ExprContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.expr`.
	 * @param ctx the parse tree
	 */
	exitExpr?: (ctx: ExprContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.templateRole`.
	 * @param ctx the parse tree
	 */
	enterTemplateRole?: (ctx: TemplateRoleContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.templateRole`.
	 * @param ctx the parse tree
	 */
	exitTemplateRole?: (ctx: TemplateRoleContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.roleAlias`.
	 * @param ctx the parse tree
	 */
	enterRoleAlias?: (ctx: RoleAliasContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.roleAlias`.
	 * @param ctx the parse tree
	 */
	exitRoleAlias?: (ctx: RoleAliasContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.roleId`.
	 * @param ctx the parse tree
	 */
	enterRoleId?: (ctx: RoleIdContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.roleId`.
	 * @param ctx the parse tree
	 */
	exitRoleId?: (ctx: RoleIdContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.accountNumber`.
	 * @param ctx the parse tree
	 */
	enterAccountNumber?: (ctx: AccountNumberContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.accountNumber`.
	 * @param ctx the parse tree
	 */
	exitAccountNumber?: (ctx: AccountNumberContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.extNum`.
	 * @param ctx the parse tree
	 */
	enterExtNum?: (ctx: ExtNumContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.extNum`.
	 * @param ctx the parse tree
	 */
	exitExtNum?: (ctx: ExtNumContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.extId`.
	 * @param ctx the parse tree
	 */
	enterExtId?: (ctx: ExtIdContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.extId`.
	 * @param ctx the parse tree
	 */
	exitExtId?: (ctx: ExtIdContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.quotedStr`.
	 * @param ctx the parse tree
	 */
	enterQuotedStr?: (ctx: QuotedStrContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.quotedStr`.
	 * @param ctx the parse tree
	 */
	exitQuotedStr?: (ctx: QuotedStrContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.assetTypeId`.
	 * @param ctx the parse tree
	 */
	enterAssetTypeId?: (ctx: AssetTypeIdContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.assetTypeId`.
	 * @param ctx the parse tree
	 */
	exitAssetTypeId?: (ctx: AssetTypeIdContext) => void;

	/**
	 * Enter a parse tree produced by `AxeTemplateParser.signature`.
	 * @param ctx the parse tree
	 */
	enterSignature?: (ctx: SignatureContext) => void;
	/**
	 * Exit a parse tree produced by `AxeTemplateParser.signature`.
	 * @param ctx the parse tree
	 */
	exitSignature?: (ctx: SignatureContext) => void;
}

