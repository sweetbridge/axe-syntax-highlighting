// Generated from syntaxes/grammars/AxeTemplate.g4 by ANTLR 4.9.0-SNAPSHOT


import { ParseTreeVisitor } from "antlr4ts/tree/ParseTreeVisitor";

import { TemplateContext } from "./AxeTemplateParser";
import { HeaderContext } from "./AxeTemplateParser";
import { MetaDefContext } from "./AxeTemplateParser";
import { MetaRoleDefContext } from "./AxeTemplateParser";
import { MetaVarDefContext } from "./AxeTemplateParser";
import { MetaVarNameContext } from "./AxeTemplateParser";
import { VarTypeContext } from "./AxeTemplateParser";
import { SrcItemContext } from "./AxeTemplateParser";
import { CommentItemContext } from "./AxeTemplateParser";
import { LabelItemContext } from "./AxeTemplateParser";
import { VarDeclItemContext } from "./AxeTemplateParser";
import { VarNameContext } from "./AxeTemplateParser";
import { VarInitExprContext } from "./AxeTemplateParser";
import { RoleVarItemContext } from "./AxeTemplateParser";
import { IncludeItemContext } from "./AxeTemplateParser";
import { LocalScopeContext } from "./AxeTemplateParser";
import { RoleLocalScopeContext } from "./AxeTemplateParser";
import { VarLocalScopeContext } from "./AxeTemplateParser";
import { AccountStmtContext } from "./AxeTemplateParser";
import { AccountSetterListContext } from "./AxeTemplateParser";
import { AccountSetterContext } from "./AxeTemplateParser";
import { AccountIdContext } from "./AxeTemplateParser";
import { CommodityStmtContext } from "./AxeTemplateParser";
import { CommodityStmtAssetContext } from "./AxeTemplateParser";
import { DiscreteStmtContext } from "./AxeTemplateParser";
import { DiscreteStmtAssetContext } from "./AxeTemplateParser";
import { AssetAdjustmentContext } from "./AxeTemplateParser";
import { AssetIdContext } from "./AxeTemplateParser";
import { ValueContext } from "./AxeTemplateParser";
import { UnitContext } from "./AxeTemplateParser";
import { QtyContext } from "./AxeTemplateParser";
import { ShareContext } from "./AxeTemplateParser";
import { NumVarExprContext } from "./AxeTemplateParser";
import { StrVarExprContext } from "./AxeTemplateParser";
import { ExprContext } from "./AxeTemplateParser";
import { TemplateRoleContext } from "./AxeTemplateParser";
import { RoleAliasContext } from "./AxeTemplateParser";
import { RoleIdContext } from "./AxeTemplateParser";
import { AccountNumberContext } from "./AxeTemplateParser";
import { ExtNumContext } from "./AxeTemplateParser";
import { ExtIdContext } from "./AxeTemplateParser";
import { QuotedStrContext } from "./AxeTemplateParser";
import { AssetTypeIdContext } from "./AxeTemplateParser";
import { SignatureContext } from "./AxeTemplateParser";


/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by `AxeTemplateParser`.
 *
 * @param <Result> The return type of the visit operation. Use `void` for
 * operations with no return type.
 */
export interface AxeTemplateVisitor<Result> extends ParseTreeVisitor<Result> {
	/**
	 * Visit a parse tree produced by `AxeTemplateParser.template`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTemplate?: (ctx: TemplateContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.header`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitHeader?: (ctx: HeaderContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.metaDef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitMetaDef?: (ctx: MetaDefContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.metaRoleDef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitMetaRoleDef?: (ctx: MetaRoleDefContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.metaVarDef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitMetaVarDef?: (ctx: MetaVarDefContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.metaVarName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitMetaVarName?: (ctx: MetaVarNameContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.varType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVarType?: (ctx: VarTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.srcItem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSrcItem?: (ctx: SrcItemContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.commentItem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommentItem?: (ctx: CommentItemContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.labelItem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitLabelItem?: (ctx: LabelItemContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.varDeclItem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVarDeclItem?: (ctx: VarDeclItemContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.varName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVarName?: (ctx: VarNameContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.varInitExpr`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVarInitExpr?: (ctx: VarInitExprContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.roleVarItem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRoleVarItem?: (ctx: RoleVarItemContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.includeItem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIncludeItem?: (ctx: IncludeItemContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.localScope`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitLocalScope?: (ctx: LocalScopeContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.roleLocalScope`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRoleLocalScope?: (ctx: RoleLocalScopeContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.varLocalScope`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVarLocalScope?: (ctx: VarLocalScopeContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.accountStmt`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAccountStmt?: (ctx: AccountStmtContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.accountSetterList`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAccountSetterList?: (ctx: AccountSetterListContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.accountSetter`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAccountSetter?: (ctx: AccountSetterContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.accountId`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAccountId?: (ctx: AccountIdContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.commodityStmt`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommodityStmt?: (ctx: CommodityStmtContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.commodityStmtAsset`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommodityStmtAsset?: (ctx: CommodityStmtAssetContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.discreteStmt`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDiscreteStmt?: (ctx: DiscreteStmtContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.discreteStmtAsset`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDiscreteStmtAsset?: (ctx: DiscreteStmtAssetContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.assetAdjustment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAssetAdjustment?: (ctx: AssetAdjustmentContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.assetId`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAssetId?: (ctx: AssetIdContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.value`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitValue?: (ctx: ValueContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.unit`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUnit?: (ctx: UnitContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.qty`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQty?: (ctx: QtyContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.share`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitShare?: (ctx: ShareContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.numVarExpr`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNumVarExpr?: (ctx: NumVarExprContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.strVarExpr`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitStrVarExpr?: (ctx: StrVarExprContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.expr`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExpr?: (ctx: ExprContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.templateRole`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTemplateRole?: (ctx: TemplateRoleContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.roleAlias`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRoleAlias?: (ctx: RoleAliasContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.roleId`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRoleId?: (ctx: RoleIdContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.accountNumber`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAccountNumber?: (ctx: AccountNumberContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.extNum`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExtNum?: (ctx: ExtNumContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.extId`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExtId?: (ctx: ExtIdContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.quotedStr`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQuotedStr?: (ctx: QuotedStrContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.assetTypeId`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAssetTypeId?: (ctx: AssetTypeIdContext) => Result;

	/**
	 * Visit a parse tree produced by `AxeTemplateParser.signature`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSignature?: (ctx: SignatureContext) => Result;
}

