import { ANTLRErrorListener, ParserRuleContext } from 'antlr4ts';
import { Diagnostic, Position, Range } from 'vscode';
import * as axe_listener from './grammar/AxeTemplateListener';
import { AccountNumberContext, AssetIdContext, AssetTypeIdContext, CommodityStmtAssetContext, MetaDefContext, MetaRoleDefContext, MetaVarDefContext, MetaVarNameContext, QtyContext, RoleIdContext, ShareContext, TemplateRoleContext, UnitContext, VarNameContext, VarTypeContext } from './grammar/AxeTemplateParser';
import * as semantics from './semantics';
import * as antlr4ts from "antlr4ts";
import * as completion from './completion';
import * as vscode from 'vscode';
import * as hover from './hover';

// ParseListener used for semantics. Detects enter/exit actions by the parser when
// delving into the generated code from AxeTemplate.g4

const metaDefRegex = /^((@[A-Z]+)|([a-z0-9_]+))([A-Z-]+)?([\`\"\']?)([^\"\'\`\n]*)([\`\"\']?)/;
export var metaVarPositions:vscode.Position[] = [];
export var metaNames:string[] = [];

export class ParseListener implements axe_listener.AxeTemplateListener
{
    // Utility method. Used by most listener functions below
    // Using the context, pushes the new semantic token structure into the builder
    push(context:ParserRuleContext, tokenType:string)
    {
        semantics.builder.push(
            new Range(
                new Position(context._start.line-1, context._start.charPositionInLine),
                new Position(context._stop!.line-1, context._stop!.charPositionInLine + context.text.length)
            ),
            tokenType,
            ["declaration"]
        );
    }
    
    // Note that all context.start.line are 1-based line numbers.

    // #region Semantic building listener override methods
    exitVarName(context:VarNameContext)
    {
        this.push(context, "variable");
    }
    
    exitQty(context:QtyContext)
    {
        this.push(context, "property");
    }

    exitShare(context:ShareContext)
    {
        this.push(context, "property");
    }

    exitUnit(context:UnitContext)
    {
        this.push(context, "property");
    }

    exitAssetId(context:AssetIdContext)
    {
        this.push(context, "property");
    }

    exitCommodityStmtAsset(context:CommodityStmtAssetContext)
    {
        this.push(context, "enumMember");
    }

    exitTemplateRole(context:TemplateRoleContext)
    {
        this.push(context, "keyword");
    }

    // At account numbers, we not only need to highlight them but also need to save them to the array of accountNumbers
    // Used by hover.ts and the hover provider
    exitAccountNumber(context:AccountNumberContext)
    {
        hover.accountNumberPositions.push(new hover.AccountNumber(
            new Position(context.start.line-1, context.start.charPositionInLine),
            context.text
        ));
        this.push(context, "class");
    }

    exitAssetTypeId(context:AssetTypeIdContext)
    {
        this.push(context, "enumMember");
    }

    // Don't need to provide highlighting here, can do it via TextMate.
    // Need to save the positions to the static array for completion.ts
    enterTemplateRole(context:TemplateRoleContext)
    {
        if (context.start.charPositionInLine === 0)
        {
            completion.templateRolePositions.push(new vscode.Position(context.start.line-1, context.start.charPositionInLine));
        }
    }

    exitMetaRoleDef(context:MetaRoleDefContext)
    {
        var pattern = /(@[a-zA-Z0-9_]+)\`|\"|'/;
        var match = context.text.match(pattern);

        if (match === null)
        {
            return;
        }
        metaNames.push(match[1]);
        metaVarPositions.push(new vscode.Position(context.start.line-1, context.start.charPositionInLine+1));
    }

    exitMetaVarName(context:MetaVarNameContext)
    {
        metaNames.push(context.text);
        metaVarPositions.push(new vscode.Position(context.start.line-1, context.start.charPositionInLine));
    }

    // Variable definitions
    exitMetaDef(context:MetaDefContext)
    {

        // Parse the text into groups, splitting by #declare
        var defs:string[] = context.text.split("#declare");
        for (var i=1; i<defs.length; i++)
        {
            var def = defs[i];
            var groups = def.match(metaDefRegex);

            if (groups === null)
            {
                continue;
            }

            // Parse the groups

            var name = groups[1];
            var type = groups[4];
            var comment = groups[6];
            var position:vscode.Position;

            for (var j=0; j<metaVarPositions.length; j++)
            {
                if (name === metaNames[j])
                {
                    position = metaVarPositions[j];
                }
            }

            if (name.indexOf("@") !== -1)
            {
                name = name.substring(1);
            }
            var metaDef = new hover.MetaDef(
                position!,
                name,
                type,
                comment
            );

            hover.metaDefs.push(metaDef);
        }
    }

    // #endregion
}

// ErrorListener used for diagnostics. Detects syntax errors
// Listener for both lexer and parser, otherwise it will not capture all issues.
export class ErrorParserListener implements ANTLRErrorListener<any>
{
    // This method is called by the client when a syntax error is detected
    syntaxError<T extends any>(recognizer: antlr4ts.Recognizer<T, any>, offendingSymbol: T | undefined, line: number, charPositionInLine: number, msg: string, e: antlr4ts.RecognitionException | undefined): void
    {
        // Declare the diagnostic via the params of this method
        var diagnostic:AxeDiagnostic = new AxeDiagnostic
        (
            new Range
            (
                new Position(line-1, charPositionInLine),
                new Position(line-1, charPositionInLine + 1)
            ),
            msg
        );

        // Check if diagnostic already exists
        // Diagnostic will already exist if multiple syntax errors occur on the same line
        for(var i:number = 0; i<semantics.diagnostics.length; i++)
        {
            let thatDiagnostic = semantics.diagnostics[i];
            if (diagnostic.equals(thatDiagnostic))
            {
                return;
            }
        }

        // Push the new diagnostic to the list
        semantics.diagnostics.push(diagnostic);
    }
}

// Subclass of vscode.Diagnostic for axe files to override equals
export class AxeDiagnostic extends Diagnostic
{
    // Equals method
    // Used so there are not multiple of the same diagnostic on the same line
    public equals(that:Diagnostic) : boolean
    {
        return this.range.isEqual(that.range) &&
            this.message === that.message;
    }
}