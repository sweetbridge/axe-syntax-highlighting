/* eslint-disable @typescript-eslint/naming-convention */
import * as vscode from 'vscode';
import * as path from 'path';
import * as fileSystem from 'fs';
import * as hover from './hover';
import * as semantics from './semantics';

// Predefined static list of vartypes for completion
const varTypeCompletionList = new vscode.CompletionList([
    new vscode.CompletionItem("TEXT"),
    new vscode.CompletionItem("NUM"),
    new vscode.CompletionItem("PERCENT"),
    new vscode.CompletionItem("D-TYPE"),
    new vscode.CompletionItem("C-TYPE"),
    new vscode.CompletionItem("UNIT")
]);

// Store positions of the templateRoles, so I can only provide completion on lines with these
export var templateRolePositions:vscode.Position[] = [];

export class AxeCompletionItemProvider implements vscode.CompletionItemProvider
{
    // This method is called whenever the user types a char in the list specified in extension.ts
    // It returns a list of CompletionItems based on the input, context, etc.
    provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext): vscode.ProviderResult<vscode.CompletionItem[] | vscode.CompletionList<vscode.CompletionItem>>
    {
        var line = document.getText().split("\n")[position.line];

        // Hack to insert second quote-like char
        // First check if need to wrap quote-like characters
        if (context.triggerCharacter === "'" || context.triggerCharacter === "`" || context.triggerCharacter === "\"")
        {
            // Insert the matching character to wrap the string
            vscode.window.activeTextEditor?.edit(editBuilder => {
                editBuilder.insert(position, context.triggerCharacter!);
            });

            // Move the cursor inside the quotes
            var newSel = new vscode.Selection(position, position);
            vscode.window.activeTextEditor!.selection = newSel;
            return;
        }

        // Update document regions (needed if the instigating key press updated the doc)
        semantics.AxeSemanticTokensProvider.updateRegions(document.getText());

        // If necessary, you can get the absolute path of the current document via
        // document.uri.fsPath

        if (context.triggerCharacter === ':')
        {
            var completionList = new vscode.CompletionList();
            for (var i=0; i<hover.metaDefs.length; i++)
            {
                var metaDef = hover.metaDefs[i];
                if (metaDef.isVar)
                {
                    var item = new vscode.CompletionItem(metaDef.name);
                    item.detail = metaDef.type;
                    item.documentation = metaDef.comment;
                    completionList.items.push(item);
                }
            }

            return completionList;
        }

        if (context.triggerCharacter === '@')
        {
            var completionList = new vscode.CompletionList();
            for (var i=0; i<hover.metaDefs.length; i++)
            {
                var metaDef = hover.metaDefs[i];
                if (!metaDef.isVar)
                {
                    var item = new vscode.CompletionItem(metaDef.name);
                    item.detail = "ROLE";
                    item.documentation = metaDef.comment;
                    completionList.items.push(item);
                }
            }
            
            return completionList;
        }

        // Check for needing varType completion
        var expression = /^#declare\s+[a-zA-Z0-9_]+\s+$/;
        var match = line.match(expression);
        if (match !== null)
        {
            return varTypeCompletionList;
        }

        // All other cases

        // Only provide completion dropdown if the current cursor position is in a valid location
        for (var i=0; i<templateRolePositions.length; i++)
        {
            var pos:vscode.Position = templateRolePositions[i];

            if (pos.line === position.line)
            {
                // If the position is valid, return the pre-constructed list of accounts from initAccountsCompletionList()
                return accountsCompletionList;
            }
        }

        // If the position is invalid, return undefined.
        return;
    }
}

// Static prebuilt completion list to be used in provideCompletionItems()
export var accountsCompletionList:vscode.CompletionList;

// Parse the src/data/accounts.json file to build the list of accounts to provide as a dropdown.
export function initAccountsCompletionList()
{
    // Get the path of the accounts.json file and parse it to json
    var extensionPath:string = vscode.extensions.getExtension("Sweetbridge.axe-extension")!.extensionUri.fsPath;
	var accountsJsonContent:string = fileSystem.readFileSync(path.join(extensionPath, "src", "data", "accounts.json")).toString();
    var json:any = JSON.parse(accountsJsonContent);

    accountsCompletionList = new vscode.CompletionList();

    // Parse the json, build the CompletionList
    for (var i=0; i<json.length; i++)
    {
        var accountJson = json[i];

        var account = new Account(
            accountJson["Account"],
            accountJson["Title/Posting"],
            accountJson["Reporting Level"],
            accountJson["Cash Flow Category"],
            accountJson["Code"],
            accountJson["New Code"],
            accountJson["Description"],
            accountJson["Natural Balance"],
            accountJson["Other"],
        );

        accounts.push(account);

        // For axe files, user will only ever need completion on posting accounts
        if (account.Title_Posting === "Posting")
        {
            var completionItem = new vscode.CompletionItem(account.Account);
            completionItem.documentation = new vscode.MarkdownString
            (
                `**Type**: ${account.Cash_Flow_Category}, ${account.Natural_Balance}\n\n**Description**: ${account.Description}`
            );
            completionItem.detail = `${account.New_Code} - ${account.Account} \n\n${account.getFullAccountPath()}`;
            completionItem.insertText = account.New_Code;
            accountsCompletionList.items.push(completionItem);
        }
    }
}

// Static list of all accounts specified in src/data/accounts.json
export var accounts:Account[] = [];

// Util class, built by initAccountsCompletionList()
export class Account
{
    public Account:string;
    public Title_Posting:string;
    public Reporting_Level:string;
    public Cash_Flow_Category:string;
    public Code:string;
    public New_Code:string;
    public Description:string;
    public Natural_Balance:string;
    public Other:string;

    public Parent:Account|undefined = undefined;

    constructor(_Account:string, _Title_Posting:string, _Reporting_Level:string, _Cash_Flow_Category:string, _Code:string,
        _New_Code:string, _Description:string, _NaturalBalance:string, _Other:string)
    {
        this.Account = _Account;
        this.Title_Posting = _Title_Posting;
        this.Reporting_Level = _Reporting_Level;
        this.Cash_Flow_Category = _Cash_Flow_Category;
        this.Code = _Code;
        this.New_Code = _New_Code;
        this.Description = _Description;
        this.Natural_Balance = _NaturalBalance;
        this.Other = _Other;

        this.Parent = this.findParent(this.New_Code);
    }

    private findParent(code:string) : Account | undefined
    {
        for (var i=accounts.length-1; i>=0; i--)
        {
            var parent:Account = accounts[i];
            if (parent.Reporting_Level === (Number(this.Reporting_Level) - 1).toString())
            {
                return parent;
            }
        }

        return;
    }

    public getFullAccountPath(): string
    {
        var path:string = "";
        var account:Account|undefined = this;

        while (account !== undefined)
        {
            path = `${account.Account} / ${path}`;

            account = account.Parent;
        }

        return path.substring(0, path.length-3);
    }
}